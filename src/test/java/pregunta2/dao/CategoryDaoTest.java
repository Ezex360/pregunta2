package pregunta2.dao;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.javalite.activejdbc.Base;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import mockit.Expectations;
import mockit.Mocked;

import java.util.List;

import pregunta2.dao.CategoryDAO;
import pregunta2.model.Category;

public class CategoryDaoTest {
	
	@Mocked CategoryDAO categoryDAO;
	
	@Before
    public void before(){
    	categoryDAO = new CategoryDAO();
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/pregunta2_test", "proyecto", "felipe");
        System.out.println("UserTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("UserTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

    @Test
    public void createCategoryIfNotInDB(){
                
        boolean a = categoryDAO.createCategory("name", "description");
        

        assertTrue(a);
    }

    @Test
    public void createCategoryIfisInDB(){
                
        boolean a = categoryDAO.createCategory("name", "description");
        boolean b = categoryDAO.createCategory("name", "description");
        

        assertTrue(!b);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCategory_with_null_name() {
        String name = null;
        String description = "description";
        
        // Create the category to add in db
        Category categoryToAdd = new Category(name, description);
        
        // Add categoryToAdd in db
        
        categoryDAO.createCategory(name, description);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCategory_with_null_description() {
        String name = "name";
        String description = null;
    
        // Create the category to add in db
        Category categoryToAdd = new Category(name, description);
        // Add categoryToAdd in db
        
        categoryDAO.createCategory(name, description);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCategory_with_empty_name() {
        String name = "";
        String description = "description";
        
        // Create the category to add in db
        Category nameToAdd = new Category(name, description);
        
        // Add categoryToAdd in db
        
        categoryDAO.createCategory(name, description);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCategory_with_empty_description() {
        String name = "name";
        String description = "";
        
        // Create the category to add in db
        Category categoryToAdd = new Category(name, description);
        
        // Add categoryToAdd in db
        
        categoryDAO.createCategory(name, description);
    }

    @Test
    public void createCategoryTest() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        assertTrue(categoryDAO.existCategory("ciencia"));
    }

    @Test
    public void findByNameTest1() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        assertNotNull(categoryDAO.findByName("ciencia"));
    }

/* COMPARA OBJETOS... (IMPLEMENTAR EQUALS)
    @Test
    public void findByNameTest2() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        Category aux = categoryDAO.findByName("ciencia");
        assertEquals(aux,categoryDAO.findByName("ciencia"));
    }
*/
    @Test
    public void findByIdTest1() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        Category quest = categoryDAO.findByName("ciencia");
        assertNotNull(categoryDAO.findById(quest.getId()));
    }

/* COMPARA OBJETOS... (IMPLEMENTAR EQUALS)
    @Test
    public void findByIdTest2() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        Category quest = categoryDAO.findByName("ciencia");
        assertEquals(quest,categoryDAO.findById(quest.getId()));
    }
*/
    @Test(expected = IllegalArgumentException.class)
    public void findByName_with_null_name() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        assertEquals(null,categoryDAO.findByName(null));
    }

    @Test
    public void findById_with_null_id() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        Category categoryAux= categoryDAO.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByCategory_with_empty_name() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        Category categoryAux= categoryDAO.findByName("");
    }

    @Test
    public void existCategoryTest(){
        categoryDAO.createCategory("ciencia","cienciaDescription");
        boolean a = categoryDAO.existCategory("ciencia");
        assertTrue(a);
    }

    @Test(expected = IllegalArgumentException.class)
    public void existCategoryTest_with_null_name() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        boolean a = categoryDAO.existCategory(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void existCategoryTest_with_empty_name() {
        categoryDAO.createCategory("ciencia","cienciaDescription");
        boolean a = categoryDAO.existCategory("");
    }

    @Test
    public void getAllCategoriesTest() {
        categoryDAO.createCategory("ciencia2","cienciaDescription");
        categoryDAO.createCategory("geografia2","geografiaDescription");
        categoryDAO.createCategory("cultura2","culturaDescription");
        List<Category> categories = categoryDAO.getAllCategories();
        assertEquals("ciencia2", categories.get(categories.size()-3).getName());
        assertEquals("geografia2", categories.get(categories.size()-2).getName());
        assertEquals("cultura2", categories.get(categories.size()-1).getName());
    }

    @Test
    public void deleteCategoryTest() {
            categoryDAO.createCategory("ciencia","cienciaDescription");
            Category aux = categoryDAO.findByName("ciencia");
            categoryDAO.deleteCategory(aux.getId());
            assertNull(categoryDAO.findByName("ciencia"));
    }
}
