package pregunta2.dao;

import static org.junit.Assert.*;

import org.javalite.activejdbc.Base;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.util.List;

import pregunta2.dao.HistoryDAO;
import pregunta2.model.History;
import pregunta2.dao.UserDAO;
import pregunta2.model.User;
import pregunta2.dao.GameDAO;
import pregunta2.model.Game;
import pregunta2.dao.QuestionDAO;
import pregunta2.model.Question;

public class HistoryDaoTest {
	UserDAO userDAO;
	GameDAO gameDAO;
    HistoryDAO historyDAO;
    QuestionDAO questionDAO;
	
	@Before
    public void before(){
    	gameDAO = new GameDAO();
        userDAO = new UserDAO();
        historyDAO = new HistoryDAO();
        questionDAO = new QuestionDAO();
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/pregunta2_test", "proyecto", "felipe");
        System.out.println("HistoryTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("HistoryTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void createRecord_with_null_user() {
        Integer user = null;
        Integer game = 485;
        Integer question = 3;
        boolean isCorrect = true;
        
        // Create the History to add in db
        History historyToAdd = new History(user, game, question, isCorrect);
        
        // Add historyToAdd in db
        
        historyDAO.createRecord(user, game, question, isCorrect);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createRecord_with_null_game() {
        Integer user = 1;
        Integer game = null;
        Integer question = 3;
        boolean isCorrect = true;
        
        // Create the History to add in db
        History historyToAdd = new History(user, game, question, isCorrect);
        
        // Add historyToAdd in db
        
        historyDAO.createRecord(user, game, question, isCorrect);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createRecord_with_null_question() {
        Integer user = 1;
        Integer game = 485;
        Integer question = null;
        boolean isCorrect = true;
        
        // Create the History to add in db
        History historyToAdd = new History(user, game, question, isCorrect);
        
        // Add historyToAdd in db
        
        historyDAO.createRecord(user, game, question, isCorrect);
    }
    
    @Test
    public void createRecordTest() {
        User userAux = new User("auto","auto");
        userAux.save();
        Game gameAux = new Game(485);
        gameAux.save();
        Question quesAux= new Question("¿5+5?","1","10","3","2",2);
        quesAux.save();

        Integer user = userAux.getId();
        Integer game = gameAux.getId();
        Integer question = quesAux.getId();
        boolean isCorrect = true;
        
        boolean a = historyDAO.createRecord(user, game, question, isCorrect);
        assertTrue(a);
    } 

    @Test
    public void getAllRecords() {

        historyDAO.createRecord(100, 20, 21, true);
        historyDAO.createRecord(101, 21, 22, false);
        historyDAO.createRecord(102, 22, 23, true);

        
        List<History> records= historyDAO.getAllRecords();

        assertTrue(records.get(records.size()-3).getIsCorrect());
        assertFalse(records.get(records.size()-2).getIsCorrect());
        assertTrue(records.get(records.size()-1).getIsCorrect());
    /*    assertEquals(20,records.get(records.size()-3).getGame());
        assertEquals(21,records.get(records.size()-2).getGame());
        assertEquals(22,records.get(records.size()-1).getGame());  */
    } 

    @Test(expected = IllegalArgumentException.class)
    public void findByUserId_with_null_user() {
                
        historyDAO.findByUserId(null);
    }

    @Test
    public void findByUserIdTest() {
        User user10 = new User("Motorola","android");
        user10.save();
        User user11 = new User("Samsung","android");
        user11.save();
        historyDAO.createRecord(user10.getId(),30,40,true);
        historyDAO.createRecord(user11.getId(),31,41,true);
        historyDAO.createRecord(user10.getId(),32,42,false);
        historyDAO.createRecord(user10.getId(),33,43,true);
        
        List<History> records= historyDAO.getAllRecords();

        List<History> recordsByUserId= historyDAO.findByUserId(user10.getId());

        assertEquals(user10.getId(),recordsByUserId.get(recordsByUserId.size()-3).getUser());
        assertEquals(user10.getId(),recordsByUserId.get(recordsByUserId.size()-2).getUser());
        assertEquals(user10.getId(),recordsByUserId.get(recordsByUserId.size()-1).getUser());


        for (int i=0; i<recordsByUserId.size();i++){
            System.out.println(recordsByUserId.get(i).getId());
        }
    }

    @Test
    public void findById_with_null_Id_Test(){
        assertEquals(null,historyDAO.findById(null));     
    }

    @Test
    public void findByIdTest(){
        User user = new User("felipe","felipe");
        user.save();
        historyDAO.createRecord(user.getId(),50,51,true);
        List<History> records= historyDAO.getAllRecords();

        assertEquals(records.get(records.size()-1).getId(),historyDAO.findById(records.get(records.size()-1).getId()).getId());     
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByGameId_with_null_Id_Test(){
        historyDAO.findByGameId(null);     
    }

    @Test
    public void findByGameIdTest() {
        Game game = new Game(485);
        game.save();

        historyDAO.createRecord(151,game.getId(),55,true);
        
        List<History> records= historyDAO.getAllRecords();

        List<History> recordsByGameId= historyDAO.findByGameId(game.getId());

        assertEquals(game.getId(),recordsByGameId.get(recordsByGameId.size()-1).getGame());
        
        for (int i=0; i<recordsByGameId.size();i++){
            System.out.println(recordsByGameId.get(i).getId());
        }
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void findByQuestionId_with_null_Id_Test(){
        historyDAO.findByQuestionId(null);     
    }

    @Test
    public void findByQuestionIdTest() {
        Question question= new Question("¿20/20?","2","10","3","1",4);
        question.save();
        historyDAO.createRecord(152,123,question.getId(),true);
        
        List<History> records= historyDAO.getAllRecords();

        List<History> recordsByQuestionId= historyDAO.findByQuestionId(question.getId());

        assertEquals(question.getId(),recordsByQuestionId.get(recordsByQuestionId.size()-1).getQuestion());
        
        for (int i=0; i<recordsByQuestionId.size();i++){
            System.out.println(recordsByQuestionId.get(i).getId());
        }
    }

}    