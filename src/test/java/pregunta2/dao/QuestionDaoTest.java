package pregunta2.dao;

import static org.junit.Assert.*;

import org.javalite.activejdbc.Base;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.util.List;

import pregunta2.dao.QuestionDAO;
import pregunta2.model.Question;

public class QuestionDaoTest {
	
	QuestionDAO questionDAO;
	
	@Before
    public void before(){
    	questionDAO = new QuestionDAO();
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/pregunta2_test", "proyecto", "felipe");
        System.out.println("QuestionTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("QuestionTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

	@Test
	public void createQuestionIfNotInDB(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2", 1);
		

		assertTrue(a);
	}

	@Test
	public void createQuestionIfisInDB(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2", 1);
		boolean b = questionDAO.createQuestion("question", "answer1", "answer2", 1);
		

		assertTrue(!b);
	}

	@Test
	public void createQuestionIfIsInDB_with_same_question(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2", 1);
		boolean b = questionDAO.createQuestion("question", "answer3", "answer4", 1);
		

		assertTrue(!b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_null_question() {
		String question = null;
		String answer1 = "answer1";
		String answer2 = "answer2";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_null_answer1() {
		String question = "question";
		String answer1 = null;
		String answer2 = "answer2";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_null_answer2() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = null;
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_null_rightans() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		Integer rightans = null;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_empty_question() {
		String question = "";
		String answer1 = "answer1";
		String answer2 = "answer2";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}
	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_empty_answer1() {
		String question = "question";
		String answer1 = "";
		String answer2 = "answer2";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}
	@Test(expected = IllegalArgumentException.class)
	public void createQuestion_with_empty_answer2() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, rightans);
	}

	//--------------------Falta rightans---------------------

	@Test
	public void createQuestion2IfNotInDB(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2", "answer3", "answer4", 1);
		

		assertTrue(a);
	}

	@Test
	public void createQuestion2IfisInDB(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2","answer3","answer4", 1);
		boolean b = questionDAO.createQuestion("question", "answer1", "answer2", "answer3", "answer4" ,1);
		

		assertTrue(!b);
	}

	@Test
	public void createQuestion2IfIsInDB_with_same_question(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2","answer3","answer4", 1);
		boolean b = questionDAO.createQuestion("question", "answer5", "answer6","answer7","answer8", 1);
		

		assertTrue(!b);
	}

	@Test
	public void createQuestionIfIsInDB_with_different_long(){
				
		boolean a = questionDAO.createQuestion("question", "answer1", "answer2", 1);
		boolean b = questionDAO.createQuestion("question", "answer3", "answer4","answer5","answer6", 1);
		

		assertTrue(!b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_question() {
		String question = null;
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_answer1() {
		String question = "question";
		String answer1 = null;
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_answer2() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = null;
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}
	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_answer3() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = null;
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_answer4() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = null;
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}	

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_null_rightans() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = null;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_empty_question() {
		String question = "";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_empty_answer1() {
		String question = "question";
		String answer1 = "";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_empty_answer2() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "";
		String answer3 = "answer3";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_empty_answer3() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "";
		String answer4 = "answer4";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createQuestion2_with_empty_answer4() {
		String question = "question";
		String answer1 = "answer1";
		String answer2 = "answer2";
		String answer3 = "answer3";
		String answer4 = "";
		Integer rightans = 1;
		// Create the question to add in db
		Question questionToAdd = new Question(question, answer1, answer2, answer3, answer4, rightans);
		
		// Add questionToAdd in db
		
		questionDAO.createQuestion(question, answer1, answer2, answer3, answer4, rightans);
	}
	
	@Test
	public void createQuestionTest() {
       	questionDAO.createQuestion("¿2+2?","1","4",2);
    	assertTrue(questionDAO.existQuestion("¿2+2?","1","4",2));
	}

	@Test
	public void createQuestion2Test() {
       	questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
    	assertTrue(questionDAO.existQuestion("¿2+2?","1","4",2));
	}

	@Test
	public void findByQuestionTest() {
    	questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
    	assertNotNull(questionDAO.findByQuestion("¿2+2?"));
	}
	@Test
	public void findByQuestion2Test() {
    	questionDAO.createQuestion("¿2+2?","1","4",2);
    	assertNotNull(questionDAO.findByQuestion("¿2+2?"));
	}

	@Test
	public void findByIdTest() {
    	questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		Question quest = questionDAO.findByQuestion("¿2+2?");
    	assertNotNull(questionDAO.findById(quest.getId()));
	}

	@Test
	public void findById2Test() {
    	questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		Question quest = questionDAO.findByQuestion("¿2+2?");
    	assertNotNull(questionDAO.findById(quest.getId()));
	}


	@Test(expected = IllegalArgumentException.class)
	public void findByQuestion_with_null_question() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		Question questionAux= questionDAO.findByQuestion(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void findByQuestion2_with_null_question() {
		questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		Question questionAux= questionDAO.findByQuestion(null);
	}


	@Test(expected = IllegalArgumentException.class)
	public void findByQuestion_with_empty_question() {
		questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		Question questionAux= questionDAO.findByQuestion("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void findByQuestion2_with_empty_question() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		Question questionAux= questionDAO.findByQuestion("");
	}	


	@Test
	public void findById_with_null_id() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		assertEquals(null, questionDAO.findById(null));
	}

	@Test
	public void findById2_with_null_id() {
		questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		assertEquals(null, questionDAO.findById(null));
	}

	@Test
	public void getAllQuestionsTest() {
    	questionDAO.createQuestion("¿2+2?","1","4","3","2",2);
		questionDAO.createQuestion("¿2-2?","0","4","3","2",1);
		questionDAO.createQuestion("¿2/2?","6","4","3","1",4);
		List<Question> questions= questionDAO.getAll();
		assertEquals("¿2+2?", questions.get(questions.size()-3).getQuestion());
		assertEquals("¿2-2?", questions.get(questions.size()-2).getQuestion());
		assertEquals("¿2/2?", questions.get(questions.size()-1).getQuestion());
	}

	@Test
	public void existQuestionTest(){
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?","1","4",2);
		assertTrue(a);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_null_question() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion(null,"1","4",2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_null_answer1() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?",null,"4",2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_null_answer2() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?","1",null,2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_null_rightans() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?","1","4",null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_empty_question() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("","1","4",2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_empty_answer1() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?","","4",2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existQuestionTest_with_empty_answer2() {
		questionDAO.createQuestion("¿2+2?","1","4",2);
		boolean a = questionDAO.existQuestion("¿2+2?","1","",2);
	}

	@Test
	public void deleteQuestionTest() {
            questionDAO.createQuestion("¿2+2?","1","4",2);
            Question aux = questionDAO.findByQuestion("¿2+2?");
            questionDAO.deleteQuestion(aux.getId());
            assertNull(questionDAO.findByQuestion("¿2+2?"));
    }
}