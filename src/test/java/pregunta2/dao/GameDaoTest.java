package pregunta2.dao;

import static org.junit.Assert.*;

import org.javalite.activejdbc.Base;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.util.List;

import pregunta2.dao.GameDAO;
import pregunta2.model.Game;
import pregunta2.dao.UserDAO;
import pregunta2.model.User;

public class GameDaoTest {
	UserDAO userDAO;
	GameDAO gameDAO;
	
	@Before
    public void before(){
    	gameDAO = new GameDAO();
        userDAO = new UserDAO();
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/pregunta2_test", "proyecto", "felipe");
        System.out.println("GameTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("GameTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

    @Test
    public void createGameIfNoActive(){
        User aux = new User("felo","felipe");
        aux.save();
        boolean a = gameDAO.createGame(aux.getId());       

        assertTrue(a);
    }

    @Test
    public void createGameIfActive(){
        User aux = new User("felipe","felipe");
        aux.save();
        boolean a = gameDAO.createGame(aux.getId());       
        boolean b = gameDAO.createGame(aux.getId());
        assertTrue(!b);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createGame_with_null_id() {
                
        gameDAO.createGame(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void activeGame_with_null_id() {
                
        gameDAO.activeGame(null);
    }


    @Test
    public void getAllGamesTest() {
        User user1 = new User("joaquin","felipe");
        user1.save();
        User user2 = new User("lucas","tortuga");
        user2.save();
        User user3 = new User("pepe","pepepass");
        user3.save();

        gameDAO.createGame(user1.getId());
        gameDAO.createGame(user2.getId());
        gameDAO.createGame(user3.getId());
        
        List<Game> games= gameDAO.getAllGames();

        assertEquals(user1.getId(),games.get(games.size()-3).getUserId());
        assertEquals(user2.getId(),games.get(games.size()-2).getUserId());
        assertEquals(user3.getId(),games.get(games.size()-1).getUserId());
    } 
    
    @Test
    public void activeGameTest1(){
        User user4 = new User("joaquin","felipe");
        user4.save();
        assertNull(gameDAO.activeGame(user4.getId()));
    }

    @Test
    public void activeGameTest2(){
        User user4 = new User("joaquin","felipe");
        user4.save();
        gameDAO.createGame(user4.getId());

        assertNotNull(gameDAO.activeGame(user4.getId()));

        List<Game> games= gameDAO.getAllGames();

        assertEquals(games.get(games.size()-1).getId(),gameDAO.activeGame(user4.getId()).getId());
    }

    @Test
    public void findByIdTest(){
        User user5 = new User("felipe","felipe");
        user5.save();
        gameDAO.createGame(user5.getId());
        List<Game> games= gameDAO.getAllGames();
        assertEquals(games.get(games.size()-1).getId(),gameDAO.findById(games.get(games.size()-1).getId()).getId());     
    }

    @Test
    public void findById_with_null_Id_Test(){
        assertEquals(null,gameDAO.findById(null));     
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByUserId_with_null_id() {
                
        gameDAO.findByUserId(null);
    }

    @Test
    public void findByUserIdTest() {
        User user6 = new User("Motorola","android");
        user6.save();
        User user7 = new User("Samsung","android");
        user7.save();
        gameDAO.createGame(user6.getId());
        gameDAO.createGame(user7.getId());
        
        List<Game> games= gameDAO.getAllGames();

        games.get(games.size()-1).endGame();
        gameDAO.createGame(user7.getId());

        List<Game> gamesByUserId= gameDAO.findByUserId(user7.getId());

        assertEquals(user7.getId(),gamesByUserId.get(gamesByUserId.size()-2).getUserId());
        assertEquals(user7.getId(),gamesByUserId.get(gamesByUserId.size()-1).getUserId());
       
        for (int i=0; i<gamesByUserId.size();i++){
            System.out.println(gamesByUserId.get(i).getId());
        }
    }

    @Test
    public void deleteGame_with_null_Id_Test(){

        boolean a = gameDAO.deleteGame(null);

        assertTrue(!a);     
    }

    @Test
    public void deleteGameTest(){
        Game aux = new Game(485);
        aux.save();
        boolean a = gameDAO.deleteGame(aux.getId());

        assertTrue(a);     
    }

    @Test
    public void getActiveGameTest(){
        User user8 = new User("diego","maradona");
        user8.save();
        Game gameAux = gameDAO.getActiveGame(user8.getId());
        assertEquals(user8.getId(),gameAux.getUserId());     
    }

    @Test(expected = IllegalArgumentException.class)
    public void onFire_with_null_game_id() {
        boolean a = gameDAO.onFire(null);    
    }

    @Test
    public void onFireTest() {
        Game aux = new Game(485);
        aux.save();
        boolean a = gameDAO.onFire(aux.getId());
        assertTrue(!a);

        aux.addFire();
        boolean b = gameDAO.onFire(aux.getId());
        assertTrue(!b);

        aux.addFire();
        boolean c = gameDAO.onFire(aux.getId());
        assertTrue(!c);

        aux.addFire();
        boolean d = gameDAO.onFire(aux.getId());
        assertTrue(d);

        aux.addFire();
        boolean e = gameDAO.onFire(aux.getId());
        assertTrue(!e);
    }
}    