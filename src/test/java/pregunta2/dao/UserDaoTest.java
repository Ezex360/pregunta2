package pregunta2.dao;

import static org.junit.Assert.*;

import org.javalite.activejdbc.Base;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import pregunta2.dao.UserDAO;
import pregunta2.model.User;

public class UserDaoTest {
	
	UserDAO userDAO;
	
	@Before
    public void before(){
    	userDAO = new UserDAO();
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/pregunta2_test", "proyecto", "felipe");
        System.out.println("UserTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("UserTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

	@Test
	public void createUserIfNotInDB(){
				
		boolean a = userDAO.createUser("Joaquin", "felipe");
		

		assertTrue(a);
	}

	@Test
	public void createUserIfIsInDB(){
				
		boolean a = userDAO.createUser("Joaquin", "felipe");
		boolean b = userDAO.createUser("Joaquin", "felipe");
		

		assertTrue(!b);
	}

	@Test
	public void createUserIfIsInDB_with_same_name(){
				
		boolean a = userDAO.createUser("Joaquin", "felipe1");
		boolean b = userDAO.createUser("Joaquin", "felipe2");
		

		assertTrue(!b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createUser_with_null_name() {
		String name = null;
		String password = "felipe";
		
		// Create the User to add in db
		User userToAdd = new User(name, password);
		
		// Add userToAdd in db
		
		userDAO.createUser(name,password);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createUser_with_null_password() {
		String name = "Joaquin";
		String password = null;
		
		// Create the User to add in db
		User userToAdd = new User(name, password);
		
		// Add userToAdd in db
		
		userDAO.createUser(name,password);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createUser_with_empty_name() {
		String name = "";
		String password = "felipe";
		
		// Create the User to add in db
		User userToAdd = new User(name, password);
		
		// Add userToAdd in db
		
		userDAO.createUser(name,password);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createUser_with_empty_password() {
		String name = "Joaquin";
		String password = "";
		
		// Create the User to add in db
		User userToAdd = new User(name, password);
		
		// Add userToAdd in db
		
		userDAO.createUser(name,password);
	}

	@Test
	public void createUserTest() {
       	userDAO.createUser("Pepe","pepepass");
    	assertTrue(userDAO.existUser("Pepe","pepepass"));
	}

	@Test
	public void findByUsernameTest() {
    	userDAO.createUser("Pepe", "pepepass");
    	assertNotNull(userDAO.findByUsername("Pepe"));
	}

	@Test
	public void findByIdTest() {
    	userDAO.createUser("Pepe", "pepepass");
		User user = userDAO.findByUsername("Pepe");
    	assertNotNull(userDAO.findById(user.getId()));
	}

	@Test
	public void getAllUsersTest() {
    	userDAO.createUser("Pepe", "pepepass");
		userDAO.createUser("Mujica", "mujicapass");
		userDAO.createUser("Uruguay", "uruguaypass");
		assertEquals("Pepe", userDAO.getAllUsers().get(0).getUsername());
		assertEquals("Mujica", userDAO.getAllUsers().get(1).getUsername());
		assertEquals("Uruguay", userDAO.getAllUsers().get(2).getUsername());
	}

	@Test
	public void findById_with_null_id() {
		userDAO.createUser("Pepe", "pepepass");
		assertEquals(null, userDAO.findById(null));
	}

	@Test(expected = IllegalArgumentException.class)
	public void findByUsername_with_null_username() {
		userDAO.createUser("Pepe", "pepepass");
		User userAux= userDAO.findByUsername(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void findByUsername_with_empty_username() {
		userDAO.createUser("Pepe", "pepepass");
		User userAux= userDAO.findByUsername("");
	}

	@Test
	public void existUserTest(){
		userDAO.createUser("Joaquin","felipe");
		boolean a = userDAO.existUser("Joaquin","felipe");
		assertTrue(a);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existUserTest_with_null_name() {
		userDAO.createUser("Joaquin","felipe");
		boolean a = userDAO.existUser(null,"felipe");
	}

	@Test(expected = IllegalArgumentException.class)
	public void existUserTest_with_null_password() {
		userDAO.createUser("Joaquin","felipe");
		boolean a = userDAO.existUser("Joaquin",null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existUserTest_with_empty_name() {
		userDAO.createUser("Joaquin","felipe");
		boolean a = userDAO.existUser("",null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existUserTest_with_empty_password() {
		userDAO.createUser("Joaquin","felipe");
		boolean a = userDAO.existUser("Joaquin","");
	}

	@Test
	public void deleteUserTest() {
            userDAO.createUser("Joaquin","felipe");
            User aux = userDAO.findByUsername("Joaquin");
            userDAO.deleteUser(aux.getId());
            assertNull(userDAO.findByUsername("Joaquin"));        
    }
}