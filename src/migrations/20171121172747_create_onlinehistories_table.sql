CREATE TABLE online_histories (
  id  int(11) NOT NULL auto_increment PRIMARY KEY,
  player1_id int(11),
  player2_id int(11),
  question_id int(11),
  isCorrect_player1 boolean,
  isCorrect_player2 boolean,
  created_at DATETIME,
  updated_at DATETIME
)ENGINE=InnoDB;
