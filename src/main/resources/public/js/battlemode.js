if (location.protocol != 'https:')
	var webSocket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/battlesocket");
else
	var webSocket = new WebSocket("wss://" + location.hostname + ":" + location.port + "/battlesocket");
webSocket.onopen = function () { 
	showMessage("Conectando con el servidor");
	console.log('conectado');
	loadInfo();
	addMeToQueue();
};
webSocket.onmessage = function (msg) {console.log(msg); update(msg); };
webSocket.onclose = function () { 
	showMessage("Has sido desconectado del servidor");
	id("timer").innerHTML+='<form action="/" method="get">'+
	'<input class="button" type="submit" value="Volver al menu"> </form>';
};

var userId;
var userName;

var	timeLeft;
var	timerId;
var instance = 1;

function clean(){
	id("question").innerHTML="";
	id("options").innerHTML="";
	id("timer").innerHTML="";
}

function loadInfo(){
	userId = id("userId").innerHTML;
	userName = id("userName").innerHTML;
}

function addMeToQueue(){
	var send = {
		"type" : "addToQueue",
		"user_id" : userId,
		"user_name" : userName
	}
	webSocket.send(JSON.stringify(send));
	delete send;
}

function showMessage(msg){
	clean();
	id("question").innerHTML=msg;
}

function makeQuestion(data){
	clearTimeout(timerId);
	id("question").innerHTML=data.question;
	id("options").innerHTML+="<input class=\"button\" type=\"submit\""+
	"value='"+data.answer1+"' id=\"1\" onclick=\"getAnswer(this.id)\">"
	id("options").innerHTML+="<input class=\"button\" type=\"submit\""+
	"value='"+data.answer2+"' id=\"2\" onclick=\"getAnswer(this.id)\">"
	id("options").innerHTML+="<input class=\"button\" type=\"submit\""+
	"value='"+data.answer3+"' id=\"3\" onclick=\"getAnswer(this.id)\">"
	id("options").innerHTML+="<input class=\"button\" type=\"submit\""+
	"value='"+data.answer4+"' id=\"4\" onclick=\"getAnswer(this.id)\">"
	timeLeft = 10;
	timerId = setInterval(countdown, 1000);
	instance++;
}

function getAnswer(answer){
	clearTimeout(timerId);
	var send = {
		"type" : "answer",
		"option" : parseInt(answer)
	}
	webSocket.send(JSON.stringify(send));
	delete send;
}

function showResult(data){
	var result = data.result;
	console.log(result);
	if(result == "BothWrong")
		showMessage("Ambos se equivocaron");
	else if(result == "BothCorrect")
		showMessage("Ambos acertaron!")
	else 
		showMessage(result+" respondio correctamente");
}

function showEndButtons(){
	id("timer").innerHTML+='<input class="button" type="submit" value="Jugar denuevo" onclick="addMeToQueue()">';
	id("timer").innerHTML+='<form action="/" method="get">'+
	'<input class="button" type="submit" value="Volver al menu"> </form>';
}

function showEnd(data){
	var result = data.result;
	console.log(result);
	if(result == "Draw")
		showMessage("Empate!");
	else 
		showMessage(result+" gano la partida!");
	showEndButtons();
}



function update(msg){
	showMessage("Cargando...");
	var data = JSON.parse(msg.data);
	if(data.type == "showQuestion"){
		makeQuestion(JSON.parse(data.question));
		console.log('-+-+-+ Preguntando -+-+-+');
	}
	if(data.type == "showResult"){
		showResult(data);
		setTimeout( function (){
			var send = {
				"type" : "question"
			}
			webSocket.send(JSON.stringify(send));
			delete send;
		},1500);
	}
	if(data.type == "alertDisconnection"){
		showMessage("Tu oponente "+data.user_disconnected+" se ha desconectado :c");
		clearTimeout(timerId);
		showEndButtons();
	}
	if(data.type == "GameEnd")
		showEnd(data);
	if(data.type == "waitOnQueque")
		showMessage("Buscando oponentes...")

}

function countdown() {
  if (timeLeft == 0) {
    getAnswer("5");
  } else {
    id("timer").innerHTML = timeLeft + 's Restantes';
    timeLeft--;
  }
}

//Helper function for selecting element by id
function id(id) {
    return document.getElementById(id);
}