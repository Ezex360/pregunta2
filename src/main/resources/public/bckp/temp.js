$(document).ready(function(){
	$('#input').click(function() {
		var send = $("#form").formToJSON();
		$.ajax({
			url: "/sign_up",
			contentType: "application/json",
			dataType: "json",
			type: "POST",
			data: send,
			error: function(xhr, error) {
				alert('Error!  Status = ' + xhr.status + ' Message = ' + error);
			},
			success: function(data) {
				
			}
		});
		return false; 
	});
});

$.fn.formToJSON = function() {
	var objectGraph = {};
	function add(objectGraph, name, value) {
		if(name.length == 1) {
			//if the array is now one element long, we're done
			objectGraph[name[0]] = value;
		}
		else {
			//else we've still got more than a single element of depth
			if(objectGraph[name[0]] == null) {
				//create the node if it doesn't yet exist
				objectGraph[name[0]] = {};
			}
		//recurse, chopping off the first array element
			add(objectGraph[name[0]], name.slice(1), value);
		}
	};
	//loop through all of the input/textarea elements of the form
	//this.find('input, textarea').each(function() {
	$(this).children('input, textarea').each(function() {
		//ignore the submit button
		if($(this).attr('name') != 'submit') {
			//split the dot notated names into arrays and pass along with the value
			add(objectGraph, $(this).attr('name').split('.'), $(this).val());
		}
	});
	$(this).children('div').each(function() {
    $(this).children('input, textarea').each(function() {
      //ignore the submit button
      if($(this).attr('name') != 'submit') {
        //split the dot notated names into arrays and pass along with the value
        add(objectGraph, $(this).attr('name').split('.'), $(this).val());
      }
    });
  });
	document.getElementById("result").innerHTML=(JSON.stringify(objectGraph));
	return JSON.stringify(objectGraph);
};

			