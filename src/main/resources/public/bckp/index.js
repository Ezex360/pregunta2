$(document).ready(function(){
  $('#submit').click(function(e) {
    e.preventDefault();
    var send = $("#register").formToJSON();
    $.ajax({
      url: "/sign_up",
      contentType: "application/json",
      dataType: "json",
      type: "POST",
      data: send,
      error: function(xhr, error) {
        var data = JSON.parse(xhr.responseText);
        id("result").innerHTML="<h1>"+data.error+"</h1>";
      },
      success: function(data) {
        id("result").innerHTML="<h1>Usuario Creado Correctamente</h1>";
      }
    });
    return false; 
  });
});

$(document).ready(function(){
  $('#enter').click(function(e) {
    e.preventDefault();
    var send = $("#log_in").formToJSON();
    $.ajax({
      url: "/sign_in",
      contentType: "application/json",
      dataType: "json",
      type: "POST",
      data: send,
      error: function(xhr, error) {
        var data = JSON.parse(xhr.responseText);
        alert(data.error);
      },
      success: function(data) {
        window.location = "http://"+location.hostname+":"+location.port+"/menu.html";
      }
    });
    return false; 
  });
});

$.fn.formToJSON = function() {
  var objectGraph = {};
  function add(objectGraph, name, value) {
    if(name.length == 1) {
      //if the array is now one element long, we're done
      objectGraph[name[0]] = value;
    }
    else {
      //else we've still got more than a single element of depth
      if(objectGraph[name[0]] == null) {
        //create the node if it doesn't yet exist
        objectGraph[name[0]] = {};
      }
    //recurse, chopping off the first array element
      add(objectGraph[name[0]], name.slice(1), value);
    }
  };
  //loop through all of the input/textarea elements of the form
  //this.find('input, textarea').each(function() {
  $(this).children('input, textarea').each(function() {
    //ignore the submit button
    if($(this).attr('name') != 'submit') {
      //split the dot notated names into arrays and pass along with the value
      add(objectGraph, $(this).attr('name').split('.'), $(this).val());
    }
  });
  $(this).children('div').each(function() {
    $(this).children('input, textarea').each(function() {
      //ignore the submit button
      if($(this).attr('name') != 'submit') {
        //split the dot notated names into arrays and pass along with the value
        add(objectGraph, $(this).attr('name').split('.'), $(this).val());
      }
    });
  });
  //document.getElementById("result").innerHTML=(JSON.stringify(objectGraph));
  return JSON.stringify(objectGraph);
};

$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

function id(id) {
    return document.getElementById(id);
}
