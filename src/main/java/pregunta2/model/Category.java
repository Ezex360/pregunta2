package pregunta2.model;

import org.javalite.activejdbc.Model;

public class Category extends Model {

	/**
   * ActiveJDBC validators of class Category
   */
  static{
    validatePresenceOf("cat_name").message("Please, provide the category name");
  }

  /**
   * Category default constructor
   */
  public Category(){

  }

  /**
   * Constructor for the class Category, takes as parameter a name
   * @param  name String that represents the name of the category
   */
  public Category(String name){
  	this.set("cat_name",name);
  }

  /**
   * Constructor for the class Category, takes as parameters 
   * a name and a description.
   * @param  name String that represents the name of the category
   * @param description String that represents the description of the category
   */
  public Category(String name, String description){
		this.set("cat_name",name);
		this.set("description",description);
  	this.save();
  }

  /**
   * @return the category id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }

  /**
   * @return the category name.
   */
  public String getName(){
      return this.getString("cat_name");
  }

  /**
   * @return the category description.
   */
  public String getDescription(){
      return this.getString("description");
  }

  /**
   * Method to define the category name.
   * @param name  String representing the category name.
   */
  public void setName(String name){
      this.set("cat_name",name);
  }

  /**
   * Method to define the category description.
   * @param description  String representing the description of the category.
   */
  public void setDescription(String description){
      this.set("description",description);
  }

}
