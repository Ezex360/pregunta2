package pregunta2.model;

import org.javalite.activejdbc.Model;

public class OnlineHistory extends Model {

	/**
   * ActiveJDBC validators of class OnlineHistory
   */
	static{
		validatePresenceOf("player1_id").message("Please, provide the player1_id");
    validatePresenceOf("player2_id").message("Please, provide the player2_id");
		validatePresenceOf("isCorrect_player1").message("Please, provide the result of player1");
    validatePresenceOf("isCorrect_player2").message("Please, provide the result of player2");
    validatePresenceOf("question_id").message("Please, provide the question_id");
	}

  /**
   * OnlineHistory default constructor
   */
  public OnlineHistory(){

  }

	/**
   * Constructor of the class OnlineHistory, take as parameters
   * an user id, a game id,a question id and the number of the answer choosen.
   * <p>
   * @param  user_id  Integer representing id of the user.
   * @param  game_id  Integer representing id of the game.
   * @param  question_id  Integer representing id of the question.
   * @param  answer1  Boolean representing the correctness of the answer choosen 
   *                 for the user1, in that game, for that question.
   * @param  answer2  Boolean representing the correctness of the answer choosen 
   *                 for the user2, in that game, for that question.               
   */
	public OnlineHistory(Integer player1_id, Integer player2_id, Integer question_id, Boolean answer1, Boolean answer2){
		this.set("player1_id",player1_id);
		this.set("player2_id",player2_id);
		this.set("question_id",question_id);
		this.set("isCorrect_player1",answer1);
    this.set("isCorrect_player2",answer2);
	}

	/**
   * @return the record id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }

  /**
   * @return the user1 id.
   */
  public Integer getUser1(){
      return this.getInteger("player1_id");
  }

  /**
   * @return the user2 id.
   */
  public Integer getUser2(){
      return this.getInteger("player2_id");
  }

  /**
   * @return the question id.
   */
  public Integer getQuestion(){
      return this.getInteger("question_id");
  }

  /**
   * @return the isCorrect field.
   */
  public boolean getIsCorrect1(){
      return this.getBoolean("isCorrect_player1");
  }

  /**
   * @return the isCorrect field.
   */
  public boolean getIsCorrect2(){
      return this.getBoolean("isCorrect_player2");
  }

}
