package pregunta2.model;

import org.javalite.activejdbc.Model;

public class Game extends Model {

	/**
   * ActiveJDBC validators of class History
   */
	static{
		validatePresenceOf("user_id").message("Please, provide the user_id");
	}

  /**
   * Game default constructor
   */
  public Game(){

  }

	/**
   * Constructor of the class Game, take as parameters
   * the user id.
   * @param  user_id  Integer representing the id of the User.
   */
  public Game(Integer user_id){
    this.set("user_id",user_id);
    this.set("consecutives",0);
  }

  /**
   * @return the category id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }
  
	/**
   * @return the category id.
   */
  public Integer getUserId(){
      return this.getInteger("user_id");
  }

  /**
   * @return the category id.
   */
  public Integer getLifes(){
      return this.getInteger("lifes");
  }

  /**
   * Method that substract 1 life.
   */
	public void substractLife(){
		Integer lifes = getLifes() - 1;
		this.set("lifes",lifes);
		this.save();
		if(lifes<=0)
			endGame();
	}

	/**
   * Method that set consecutives answers back to 0.
   */	
	public void resetFire(){
		this.set("consecutives",0);
		this.save();
	}

	/**
   * Method that adds 1 to the consecutives correct answers, except that
   * consecutives field is more than 3. In that case, the method
   * reset the number of consecutives answers.
   */	
	public void addFire(){
		Integer cons = getInteger("consecutives");
		if (cons >3)
			resetFire();
		else{
			this.set("consecutives",(this.getInteger("consecutives")+1) );
			this.save();
		}
	}
	/**
	 * @return the number of consecutives correct answers
	 */
	public boolean onFire(){
		Integer cons = this.getInteger("consecutives");
		return cons==3;
	}

	/**
	 * Method that sets the Game status to false (ends the game).
	 */
	public void endGame(){
		this.set("status",false);
		this.save();
	}	

	

}


