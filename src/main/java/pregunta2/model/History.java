package pregunta2.model;

import org.javalite.activejdbc.Model;

public class History extends Model {

	/**
   * ActiveJDBC validators of class History
   */
	static{
		validatePresenceOf("user_id").message("Please, provide the user_id");
		validatePresenceOf("game_id").message("Please, provide the game_id");
		validatePresenceOf("question_id").message("Please, provide the question_id");
		validatePresenceOf("isCorrect").message("Please, provide the result");
	}

  /**
   * History default constructor
   */
  public History(){

  }

	/**
   * Constructor of the class History, take as parameters
   * an user id, a game id,a question id and the number of the answer choosen.
   * <p>
   * @param  user_id  Integer representing id of the user.
   * @param  game_id  Integer representing id of the game.
   * @param  question_id  Integer representing id of the question.
   * @param  answer  Boolean representing the correctness of the answer choosen 
   *                 for the user, in that game, for that question.
   */
	public History(Integer user_id, Integer game_id, Integer question_id, Boolean answer){
		this.set("user_id",user_id);
		this.set("game_id",game_id);
		this.set("question_id",question_id);
		this.set("isCorrect",answer);
	}

	/**
   * @return the record id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }

  /**
   * @return the user id.
   */
  public Integer getUser(){
      return this.getInteger("user_id");
  }

  /**
   * @return the question id.
   */
  public Integer getQuestion(){
      return this.getInteger("question_id");
  }

  /**
   * @return the game id.
   */
  public Integer getGame(){
      return this.getInteger("game_id");
  }

  /**
   * @return the isCorrect field.
   */
  public boolean getIsCorrect(){
      return this.getBoolean("isCorrect");
  }

  /**
   * Method to define the user id.
   * @param user  Integer representing the user id.
   */
  public void setUser(Integer user){
      this.set("user_id",user);
  }

  /**
   * Method to define the game id.
   * @param game  Integer representing the game id.
   */
  public void setGame(Integer game){
      this.set("game_id",game);
  }

  /**
   * Method to define the question id.
   * @param question  Integer representing the question id.
   */
  public void setQuestion(Integer question){
      this.set("question_id",question);
  }

  /**
   * Method to define the correctness of the question answered by the user for the game.
   * @param answered  Boolean representing the correctness of the answer.
   */
  public void setIsCorrect(Boolean answered){
      this.set("isCorrect",answered);
  }

}
