package pregunta2.model;

import org.javalite.activejdbc.Model;

public class Question extends Model {

	/**
   * ActiveJDBC validators of class Question
   */
	static{
		validatePresenceOf("question").message("Please, provide the category name");
		validatePresenceOf("answer1").message("Please, provide the category name");
		validatePresenceOf("answer2").message("Please, provide the category name");
		validatePresenceOf("rightans").message("Please, provide the category name");
	}

  /**
   * Question default constructor
   */
  public Question(){

  }

 	/**
   * Constructor of the class Question, take as parameters
   * a question, two answers, and the number of the right answer.
   * <p>
   * @param  question  String representing question.
   * @param  answer1  String representing the first answer option.
   * @param  answer2  String representing the second answer option.
   * @param  rightans  Integer representing number of the correct answer.
   */
	public Question(String question,String answer1,String answer2,Integer rightans){
		this.set("question",question);
		this.set("answer1",answer1);
		this.set("answer2",answer2);
		this.set("rightans",rightans);
	}

	 /**
   * Constructor of the class Question, take as parameters
   * a question, two answers, and the number of the right answer.
   * <p>
   * @param  question  String representing question.
   * @param  answer1  String representing the first answer option.
   * @param  answer2  String representing the second answer option.
   * @param  answer3  String representing the third answer option.
   * @param  answer4  String representing the fourth answer option.
   * @param  rightans  Integer representing number of the correct answer.
   */
	public Question(String question,String answer1,String answer2, String answer3, String answer4, Integer rightans){
		this.set("question",question);
		this.set("answer1",answer1);
		this.set("answer2",answer2);
		this.set("answer3",answer1);
		this.set("answer4",answer2);
		this.set("rightans",rightans);
	}

	/**
   * @return the question id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }

  /**
   * @return the question description.
   */
  public String getQuestion(){
      return this.getString("question");
  }

  /**
   * @return the first answer description.
   */
  public String getAnswer1(){
      return this.getString("answer1");
  }

 /**
  * @return the second answer description.
  */
  public String getAnswer2(){
      return this.getString("answer2");
  }

 /**
  * @return the third answer description.
  */
  public String getAnswer3(){
      return this.getString("answer3");
  }

	/**
	 * @return the fourth answer description.
	 */
  public String getAnswer4(){
      return this.getString("answer4");
  }

  /**
   * @return number of the right answer.
   */
  public Integer getRightAnswer(){
      return this.getInteger("rightans");
  }
  
  /**
   * Method to define the question.
   * <p>
   * @param question  String representing the question.
   */
  public void setQuestion(String question){
      this.set("question",question);
  }

  /**
   * Method to define the first answer.
   * <p>
   * @param answer  String representing the answer.
   */
  public void setAnswer1(String answer){
      this.set("answer1",answer);
  }

  /**
   * Method to define the second answer.
   * <p>
   * @param answer  String representing the answer.
   */
  public void setAnswer2(String answer){
      this.set("answer2",answer);
  }

  /**
   * Method to define the third answer.
   * <p>
   * @param answer  String representing the answer.
   */
  public void setAnswer3(String answer){
      this.set("answer3",answer);
  }

  /**
   * Method to define the fourth answer.
   * <p>
   * @param answer  String representing the answer.
   */
  public void setAnswer4(String answer){
      this.set("answer4",answer);
  }

/**
   * Method to define the number of the right answer.
   * <p>
   * @param correctNumber  Integer representing number of the correct answer.
   */
  public void setRightAnswer(Integer correctNumber){
      this.set("rightans",correctNumber);
  }

	/**
	 * Method that compares an Integer passed as parameter with
	 * the rightans field and return true iff they are equals.
	 * @param  res Integer that represents the number of answer to compare.
	 * @return true iff res == rightans.
	 */
  public Boolean checkAnswer(Integer res){
  	return (res.equals(this.getInteger("rightans")));
  }

}
