package pregunta2.model;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.validation.UniquenessValidator;

public class User extends Model {
  
  /**
   * ActiveJDBC validators of class User
   */
  static{
    validatePresenceOf("username").message("Please, provide your username");
    validatePresenceOf("password").message("Please, provide your password");
    //validateWith(new UniquenessValidator("username")).message("This username is already taken."); ANDA MAL
  }

  /**
   * User default constructor
   */
  public User(){

  }
  
  /**
   * Constructor of the class User, take as parameters
   * the user name and a password.
   * @param  newName  String representing the user name.
   * @param  newPassword  String representing the user password.
   */
  public User(String newName, String newPassword){
    this.set("username",newName);
    this.set("password",newPassword);
    this.set("score","0");
  }

  /**
   * @return the user id.
   */
  public Integer getId(){
      return this.getInteger("id");
  }

  /**
   * @return the user name.
   */
  public String getUsername(){
      return this.getString("username");
  }

  /**
   * @return the user password.
   */
  public String getPassword(){
      return this.getString("password");
  }

  /**
   * @return the user score.
   */
  public String getScore(){
      return this.getString("score");
  }

  /**
   * Method to define the user name.
   * <p>
   * @param newName  String representing the user name.
   */
  public void setUsername(String newName){
      this.set("username",newName);
  }

  /**
   * Method to define the user password.
   * <p>
   * @param newPassword  String representing the user password.
   */
  public void setPassword(String newPassword){
      this.set("password",newPassword);
  }

  /**
   * Method to define the user score.
   * <p>
   * @param newScore  String representing the user average of correct answers.
   */
  public void setScore(String newScore){
    this.set("score",newScore);
  }


}
