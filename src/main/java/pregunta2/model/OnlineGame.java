package pregunta2.model;

import org.eclipse.jetty.websocket.api.Session;

import java.util.List;
import java.util.ArrayList;

import pregunta2.util.Pair;

public class OnlineGame {

    /**
     * Username of the player1
     */
    private Integer id;

    /**
     * Username and Id of the player1
     */
    private Pair<Integer,String> user1;
    /**
     * Username and Id of the player2
     */
    private Pair<Integer,String> user2;
    /**
     * Identification for the Session of the player1. Should not be {@code null}
     */
    private Session player1;
    /**
     * Identification for the Session of the player2. Should not be {@code null}
     */
    private Session player2;
    /**
     * Quantity of correct answers made by the player1.
     */
    private Integer correctsPlayer1;
    /**
     * Quantity of correct answers made by the player2.
     */
    private Integer correctsPlayer2;
    /**
     * Question that both players have to answer in that round.
     */
    private Question activeQuestion;
    /**
     * Represents the option choosen by the player1 for the activeQuestion 
     */
    private Integer answerPlayer1;
    /**
     * Represents the option choosen by the player2 for the activeQuestion 
     */
    private Integer answerPlayer2;
    /**
     * Number of questions done in the game so far.
     */
    private Integer done_questions;
    /**
     * Represents the status of the game (false if the game is no more active).
     */
    private boolean status;
    /**
     * List that have all the records of the answers. Should dump on database before 
     * deleting the object
     */
    private List<OnlineHistory> records;

    /**
     * Constructor of the class, take as parameters two sessions that represents
     * the addresses of the player1 and player2 of the OnlineGame and two Strings that
     * represents the usernames of the players. Also recibes the id of the game.
     * @param  id  Session representing the addresses of the player1
     * @param  one  Session representing the addresses of the player1.
     * @param  two  Session representing the addresses of the player2.
     * @param  one_name  String representing the username of the player1.
     * @param  two_name  String representing the username of the player2.
     */
    public OnlineGame(Integer game_id, Session one, Session two, Pair<Integer,String> newUser1 ,Pair<Integer,String> newUser2){
        id = game_id;
        user1 = newUser1;
        user2 = newUser2;
    	player1 = one;
    	player2 = two;
        correctsPlayer1 = 0;
        correctsPlayer2 = 0;
        answerPlayer1 = 0;
        answerPlayer2 = 0;
    	activeQuestion = null;
    	done_questions = 0;
        status = true;
        records = new ArrayList<>();
    }

    /**
     * @return the player1 session.
     */
    public Session getPlayer1(){
    	return player1;
    }

    /**
     * @return the player2 session.
     */
    public Session getPlayer2(){
        return player2;
    }

    /**
     * @return the player1 username.
     */
    public String getPlayer1Username(){
        return user1.snd();
    }

    /**
     * @return the player2 username.
     */
    public String getPlayer2Username(){
        return user2.snd();
    }

    /**
     * @return the number of correct answers of the player1.
     */
    public Integer getCorrects1(){
        return correctsPlayer1;
    }

    /**
     * @return the number of correct answers of the player2.
     */
    public Integer getCorrects2(){
        return correctsPlayer2;
    }

     /**
     * @return the game status.
     */
    public boolean getStatus(){
        return status;
    }

    /**
     * @return the game id.
     */
    public Integer getId(){
        return id;
    }

    /**
     * Method to define the answerPlayer1
     * @param result Integer that represents the option choosen by the player1
     * for the activeQuestion
     */
    public void setAnswerPlayer1(Integer result){
        answerPlayer1 = result;
    }

    /**
     * Method to define the answerPlayer2
     * @param result Integer that represents the option choosen by the player2
     * for the activeQuestion
     */
    public void setAnswerPlayer2(Integer result){
        answerPlayer2 = result;
    }

    /**
     * Method to define the activeQuestion
     * @param question to answer for both users in the round
     */
    public void setQuestion(Question question){
        activeQuestion = question;
    }

    /**
     * Check if the game has reached to the end. (has passed 10 rounds?)
     * and returns a String with the name of the winner
     * @return String that represent the winner of the game, if it exists
     */
    public String gameEnd(){
        if(done_questions < 10)
            return "NotYet";
        else{
            status = false;
            if(correctsPlayer1 > correctsPlayer2)
                return getPlayer1Username();
            else if(correctsPlayer2 > correctsPlayer1)
               return getPlayer2Username();
            else                           
                return "Draw";
        }
    }

    /**
     * Method to compare a session and see if matches with the player1 session
     * @param session to compare
     * @return true if both sessions are equal
     */
    public boolean isPlayer1(Session session){
        return player1 == session;
    }

    /**
     * Method to compare a session and see if matches with the player1 session
     * @param session to compare
     * @return true if both sessions are equal
     */
    public boolean isPlayer2(Session session){
        return player2 == session;
    }

    /**
     * Method that check the both answers of the players for the activeQuestion,
     * creates a record with the result of that round and returns a String with
     * the result.     
     * @return String that represents who win in the round
     */
    public String compareAnswers(){
        System.out.println("answerPlayer1 = "+answerPlayer1);
        System.out.println("answerPlayer2 = "+answerPlayer2);
        if (answerPlayer1==0 || answerPlayer2==0)
            return "WaitingForAnswers";
        else{
    		boolean answer1 = activeQuestion.checkAnswer(answerPlayer1);
    		boolean answer2 = activeQuestion.checkAnswer(answerPlayer2);
    		done_questions++;
            answerPlayer1=0;
            answerPlayer2=0;
            OnlineHistory record = new OnlineHistory(user1.fst(),user2.fst(),activeQuestion.getId(),answer1,answer2);
            records.add(record);
            activeQuestion = null;
    		if(answer1 && !answer2){
                correctsPlayer1++;
    			return getPlayer1Username();
    		} else if (answer2 && !answer1){
                correctsPlayer2++;
    			return getPlayer2Username();
    		} else if(!answer1 && !answer2) {
    			return "BothWrong";
    		}else{
                correctsPlayer1++;
                correctsPlayer2++;
                return "BothCorrect";
            }
        }
    }

    /**
     * Method that save all the OnlineHistory objects in the record list
     * in the database     
     * @return String that represents who win in the round
     */
    public void saveRecords(){
        for(OnlineHistory record : records){
            record.save();
        }
    }

}