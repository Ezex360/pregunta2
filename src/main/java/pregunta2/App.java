package pregunta2;

import pregunta2.model.*;
import pregunta2.controller.*;
import pregunta2.dao.*;
import pregunta2.util.*;
import pregunta2.webSocket.*;

import java.util.*; //DELETE AFTER DEVELOP

import static spark.Spark.*;

/**
 * Main Class :D
 *
 */
public class App{

    // Declare dependencies
    public static UserDAO userDao;
    public static CategoryDAO categoryDao;
    public static GameDAO gameDao;
    public static HistoryDAO historyDao;
    public static QuestionDAO questionDao;
    public static OnlineGameDAO onlineGameDao;

    public static void main( String[] args ){

      // Instantiate dependencies
      userDao = new UserDAO();
      categoryDao = new CategoryDAO();
      gameDao = new GameDAO();
      historyDao = new HistoryDAO();
      questionDao = new QuestionDAO();
      onlineGameDao = new OnlineGameDAO();

      CommandLineOptions.parseOptions(args);

      // Configure Spark
      port(ServerOptions.getInstance().getAppPort());

      /* Only for develop
			String projectDir = System.getProperty("user.dir");
      String staticDir = "/src/main/resources/public";
      staticFiles.externalLocation(projectDir + staticDir);
      */
      staticFiles.location("/public");
      webSocket("/battlesocket", BattlePlayWebSocket.class);
      webSocket("/chatsocket", ChatWebSocketHandler.class);
      init();

      get("/hello",(req, res) -> "Hello World!");
      get("/hello/:name",(req, res) -> "Hello "+req.params(":name")+"!");
      
      before((req, res)->{
        ServerOptions.getInstance().openDB();
      });

      after((req, res) -> {
        ServerOptions.getInstance().closeDB();
      });
      
      //Login API Methods
      get("/down",(req, res) ->{ ServerOptions.getInstance().closeDB(); return null;});
      get("/battleModeQueque", BattlePlay.serveBattleMode);
      get("/", LoginController.serveFrontPage);
      get("/menu", LoginController.serveFrontPage);
      get("/score", UserController.serveAverage);
      get("/ranking", UserController.serveRanking);
      post("/sign_up", LoginController.handleSignUpPost);
      post("/login", LoginController.handleLoginPost);
      get("/logout", LoginController.handleLogoutPost);
      get("/startSingleGame", SinglePlayController.startGame);
      get("/roulete", SinglePlayController.chooseCategoryToAnswer);
      get("/bonus/:category_name", SinglePlayController.getQuestionOfCategory);
      get("/play/:question_id", SinglePlayController.serveQuestion);
      get("/chat", Chat.serveChat);
      post("/play/:question_id", SinglePlayController.handleAnswerPost);

    }
}
