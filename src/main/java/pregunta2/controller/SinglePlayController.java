package pregunta2.controller;

import pregunta2.model.*;
import pregunta2.dao.*;
import pregunta2.util.*;

import pregunta2.controller.UserController;
import static pregunta2.App.userDao;
import static pregunta2.App.gameDao;
import static pregunta2.App.questionDao;
import static pregunta2.App.historyDao;
import static pregunta2.App.categoryDao;

import static pregunta2.util.RequestUtil.*;

import spark.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class SinglePlayController {

  /**
   * Recibes an active Game, a Question and a integer that represents the number of option
   * choosen as answer and returns true iff the question is well answered. Also, it saves
   * the result in the Database
   * @param  game Game, represents the active game
   * @param  question   Question, represents the active Question
   * @param  answer Integer, represents the number of the answer options choosen
   * @return     true if the Question is well Answered
   */
  public static boolean answerQuestion(Game game,Question question,Integer answer){
    Boolean isCorrect = question.checkAnswer(answer);
    boolean fire;
    if (!isCorrect){
      game.substractLife();
      game.resetFire();
    }else{
      game.addFire();
    }
    historyDao.createRecord(game.getUserId(),game.getId(),question.getId(),isCorrect);
    return isCorrect;
  }

  /**
   * Check if the the user has answered 3 questions correctly of each category.
   * @param  game to see if the user won
   * @return  true if the user won the game
   */
  public static boolean WinGame(Game game){
    boolean win = true;
    try{
      java.sql.Connection connection = org.javalite.activejdbc.Base.connection();
      String query = "select category_id from histories join questions join games on "+
      "(question_id = questions.id and game_id = games.id)"+
      " where (histories.user_id ="+game.getUserId()+" and status = true and isCorrect = true)";
      java.sql.Statement st = connection.createStatement();
      st.executeQuery(query); 
      java.sql.ResultSet resultSet = st.executeQuery(query);
      List<Integer> array = new ArrayList<Integer>();
      while (resultSet.next()) {
        array.add(resultSet.getInt(1));
      }
      Integer count = 0;
      for(int j=1;j<=6;j++){
        for(int i=0;i<array.size();i++){
          if (j==array.get(i))
            count++;
          if (count==3)
            break;
        }
        if (count<3){
          win = false;
          break;
        }
        count = 0;
      }
    } catch(java.sql.SQLException sqle) {
      sqle.printStackTrace();
      System.err.println("Error connecting: " + sqle);
    }
    if(win)
      game.endGame();
    return win;
  }


  public static Route startGame = (Request request, Response response) -> {
    if(!checkActiveUser(request)){
      response.redirect("/");
      return null;
    }
    Game activeGame = gameDao.getActiveGame(getSessionCurrentUserId(request));
    request.session().attribute("currentGameId",activeGame.getId());
    response.redirect("/roulete");
    return null;      
  };

  public static Route chooseCategoryToAnswer = (Request request, Response response) -> {
    if(!checkActiveGame(request)){
      response.redirect("/");
      return null;
    }
    if(gameDao.onFire(getSessionCurrentGameId(request)))
      return ViewUtil.render(null, "singlePlay/categories.mustache");
    else
      return ViewUtil.render(null, "singlePlay/ruleta.mustache");
  };

  public static Route getQuestionOfCategory = (Request request, Response response) -> {
    if(!checkActiveGame(request)){
      response.redirect("/");
      return null;
    }
    Integer category_id = categoryDao.findByName(request.params(":category_name")).getId();
    Question random_question = questionDao.randomQuestion(category_id,getSessionCurrentUserId(request));
    response.redirect("/play/:"+random_question.getId());
    return null; 
  };

  public static Route serveQuestion = (Request request, Response response) -> {
    if(!checkActiveGame(request)){
      response.redirect("/");
      return null;
    }
    Question question = questionDao.findById(getParamQuestionId(request));
    Map model = new HashMap();
    model.put("question",question.getString("question"));
    model.put("option1",question.getString("answer1"));
    model.put("option2",question.getString("answer2"));
    model.put("option3",question.getString("answer3"));
    model.put("option4",question.getString("answer4"));
    model.put("question_id",question.getInteger("id"));
    return ViewUtil.render(model,"singlePlay/play.mustache"); 
  };

  public static Route handleAnswerPost = (Request request, Response response) -> {
    if(!checkActiveGame(request)){
      response.redirect("/");
      return null;
    }
    Integer answer = 0;
    if(request.queryParams("option1")!=null)
      answer = 1;
    if(request.queryParams("option2")!=null)
      answer = 2;
    if(request.queryParams("option3")!=null)
      answer = 3;
    if(request.queryParams("option4")!=null)
      answer = 4;
    Question activeQuestion = questionDao.findById(getParamQuestionId(request));
    Game activeGame = gameDao.findById(getSessionCurrentGameId(request));
    boolean isCorrect = answerQuestion(activeGame,activeQuestion,answer);
    Map model = new HashMap();
    if (isCorrect){ 
      model.put("res"," Correcta!");
      model.put("lifes",activeGame.getLifes());
      if (WinGame(activeGame)){ 
        UserController.calculateScore(getSessionCurrentUserId(request));
        model.put("result","Ganaste :)");          
        return ViewUtil.render(model, "singlePlay/winnerOrLosser.mustache");
      }else
        return ViewUtil.render(model, "singlePlay/result.mustache");
    }else{
      Integer availableLifes = activeGame.getLifes();
      model.put("res"," incorrecta :(");
      model.put("lifes",availableLifes);
      if (availableLifes<=0){
        UserController.calculateScore(getSessionCurrentUserId(request));
        model.put("result","Perdiste!");          
        return ViewUtil.render(model, "singlePlay/winnerOrLosser.mustache");
      }else
        return ViewUtil.render(model, "singlePlay/result.mustache");
    }
  };

}
