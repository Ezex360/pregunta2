package pregunta2.controller;

import pregunta2.model.User;
import pregunta2.dao.UserDAO;
import pregunta2.util.*;
import static pregunta2.App.userDao;
import static pregunta2.util.RequestUtil.*;

import spark.*;
import java.util.HashMap;
import java.util.Map;


public class LoginController {

    public static Route serveFrontPage = (Request request, Response response) -> {
    if(!checkActiveUser(request))
      return ViewUtil.render(null,"index.mustache");
    else
      return ViewUtil.render(null,"menu.mustache");      
  };

    public static Route handleSignUpPost = (Request request, Response response) -> {
        Map model = new HashMap();
        Boolean userCreated = userDao.createUser(getQueryUsername(request), getQueryPassword(request));
        if(!userCreated){
            model.put("error_sign_up",true);
            return ViewUtil.render(model,"index.mustache");
        } else {
            model.put("sign_up_ok",true);
            return ViewUtil.render(model,"index.mustache");
        }
    };

    public static Route handleLoginPost = (Request request, Response response) -> {
        Map model = new HashMap();
        if (!UserController.authenticate(getQueryUsername(request), getQueryPassword(request))) {
            model.put("error_login", true);
            return ViewUtil.render(model, "index.mustache");
        }
        request.session(true);
        request.session().attribute("currentUser", getQueryUsername(request));
        Integer userId = userDao.findByUsername(getQueryUsername(request)).getId();
        request.session().attribute("currentUserId",userId);
        response.redirect("/menu");        
        return null;
    };
    public static Route handleLogoutPost = (Request request, Response response) -> {
        removeSessionAttrCurrentUser(request);
        removeSessionAttrCurrentUserId(request);
        response.redirect("/");
        return null;
    };

    /*
    // The origin of the request (request.pathInfo()) is saved in the session so
    // the user can be redirected back after login
    public static void ensureUserIsLoggedIn(Request request, Response response) {
        if (request.session().attribute("currentUser") == null) {
            request.session().attribute("loginRedirect", request.pathInfo());
            response.redirect(Path.Web.LOGIN);
        }
    };
    */

}
