package pregunta2.controller;

import pregunta2.dao.UserDAO;
import pregunta2.model.User;
import pregunta2.model.History;
import pregunta2.util.ViewUtil;
import pregunta2.util.Encryption;
import static pregunta2.App.userDao;
import static pregunta2.util.RequestUtil.*;
import static java.lang.Math.toIntExact;

import spark.*;
import com.google.gson.Gson;

import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.LinkedList;
import java.util.HashMap;


public class UserController{

	/**
	 * Method to authenticate a user. Verify that the userId is not null,
	 * that the password is not empty, that userId exists in the database
	 * and finally, that the password entered is correct.
	 * <p>
	 * @param  username  username of the user to authenticate.
	 * @param  password  password of the user to authenticate.
	 * @return {@code true} if and only if the user exists in the database
	 * and the password entered is correct.
	 */
	public static boolean authenticate(String username, String password) {
	    if (username.isEmpty() || password.isEmpty()) {
	        return false;
	    }
	    User user = userDao.findByUsername(username);
	    if (user == null) {
	        return false;
	    }
	    String decryptedPassword = "";
      try{  
        decryptedPassword = Encryption.decrypt(user.getPassword());
      }catch(Exception e){System.err.println(e);}
	    return password.equals(decryptedPassword);
	}

	/**
   * Method that calculates the average of correct answers made by an user
   * @param  user_id to calculate the prom
   * @return  String that represents the percent of correct answers
   */
  public static void calculateScore(Integer user_id) {
    if(user_id != null){
      User user = userDao.findById(user_id);
      if(user!=null){
        int totalAnswers = toIntExact(History.count("user_id = ?",user_id));
        int correctAnswers = toIntExact(History.count("user_id = ? and isCorrect = true",user_id));
        if(totalAnswers!=0){
          float prom = (float) correctAnswers /  totalAnswers;
          prom = prom * 100;
          user.setScore(String.format("%.02f", prom));
          user.save();
        }
      }
    }
  }

	public static Route serveAverage = (Request request, Response response) -> {
    if(!checkActiveUser(request)){
      response.redirect("/");
      return null;
    }
    Map model = new HashMap();
    model.put("user_score",userDao.findById(getSessionCurrentUserId(request)).getScore());
    return ViewUtil.render(model,"statistics/score.mustache");
  };

  public static Route serveRanking = (Request request, Response response) -> {
    if(!checkActiveUser(request)){
      response.redirect("/");
      return null;
    }
    Map model = new HashMap();
    model.put("ranking",userDao.getRanking());
    return ViewUtil.render(model,"statistics/ranking.mustache");
  };
 
}