package pregunta2.webSocket;

import pregunta2.ServerOptions;
import java.sql.*;
import static spark.Spark.*;
import java.util.*;
import spark.Route;
import spark.Request;
import spark.Response;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import com.google.gson.Gson;

import pregunta2.util.*;
import static pregunta2.util.RequestUtil.*;
import static pregunta2.App.onlineGameDao;
import static pregunta2.App.questionDao;
import static pregunta2.App.userDao;
import pregunta2.model.*;

public class BattlePlay {

    public static Map<Session,Pair<Integer,String>> userMap = new HashMap<>();
    public static Queue<Session> onHold = new LinkedList<>();
    static int gameNumber = 1;


    public static Route serveBattleMode = (Request request, Response response) -> {
        if(!checkActiveUser(request)){
          response.redirect("/");
          return null;
        }
        Map model = new HashMap();
        model.put("userId",getSessionCurrentUserId(request));
        model.put("userName",getSessionCurrentUser(request));
        return ViewUtil.render(model,"battleMode/battlemode.mustache");      
    };

    private static void sendMessage(Session user,JSONObject message){
        try {
            user.getRemote().sendString(String.valueOf(message));
        } catch (Exception e) { e.printStackTrace(); } 
    }

    public static void addToQueue(Session userSession, Integer userId, String userName){
        Pair<Integer,String> user = new Pair<Integer,String>(userId,userName);
        if(!userMap.containsKey(userSession))
            userMap.put(userSession,user);
        onHold.add(userSession);
        if(!makeGame()){
            JSONObject message = new JSONObject().put("type","waitOnQueque");
            sendMessage(userSession,message);
        }
    }

    public static boolean makeGame(){
    	if (onHold.size() >= 2) {
    		Session player1 = onHold.poll();
    		Session player2 = onHold.poll();
    		OnlineGame newGame = onlineGameDao.createOnlineGame(gameNumber++,player1,player2,userMap.get(player1),userMap.get(player2));
            makeQuestion(newGame);
            return true;
    	} else {
            return false;
        }
    }

    public static void makeQuestion(OnlineGame active){
        ServerOptions.getInstance().openDB();
        Question question = questionDao.randomQuestion();
        ServerOptions.getInstance().closeDB();
        active.setQuestion(question);
        JSONObject message = new JSONObject().put("type","showQuestion").put("question",question.toJson(true));
        sendMessage(active.getPlayer1(),message);
        sendMessage(active.getPlayer2(),message); 
    }

    public static void sendResult(OnlineGame active, String result){
        JSONObject message = new JSONObject().put("type","showResult").put("result",result);
        sendMessage(active.getPlayer1(),message);
        sendMessage(active.getPlayer2(),message); 
    }

    public static void sendEnd(OnlineGame active, String winner){
        JSONObject message = new JSONObject().put("type","GameEnd").put("result",winner);
        sendMessage(active.getPlayer1(),message);
        sendMessage(active.getPlayer2(),message);
        ServerOptions.getInstance().openDB();
        active.saveRecords();
        ServerOptions.getInstance().closeDB();
        onlineGameDao.deleteGame(active);

    }

    public static void alertDisconnection(OnlineGame active, Session disconnected){
        String user_disconnected="";
        if(active.isPlayer1(disconnected)){
            user_disconnected = active.getPlayer1Username();
            JSONObject message = new JSONObject().put("type","alertDisconnection").put("user_disconnected",user_disconnected);
            sendMessage(active.getPlayer2(),message);
        }
        if(active.isPlayer2(disconnected)){
            user_disconnected = active.getPlayer2Username();
            JSONObject message = new JSONObject().put("type","alertDisconnection").put("user_disconnected",user_disconnected);
            sendMessage(active.getPlayer1(),message);
        }
        ServerOptions.getInstance().openDB();
        active.saveRecords();
        ServerOptions.getInstance().closeDB();
        onlineGameDao.deleteGame(active);

    }

}