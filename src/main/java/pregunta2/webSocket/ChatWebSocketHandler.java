package pregunta2.webSocket;

import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

import org.json.*;
import com.google.gson.Gson;

@WebSocket
public class ChatWebSocketHandler {

    private String sender, msg;

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
    
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        String username = Chat.userUsernameMap.get(user);
        Chat.userUsernameMap.remove(user);
        Chat.broadcastMessage(sender = "Server", msg = (username + " left the chat"));
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        JSONObject data = new JSONObject(message);
        if(data.optString("message") != ""){
             Chat.broadcastMessage(sender = Chat.userUsernameMap.get(user), msg = data.optString("message"));
        }
        if(data.optString("username") != ""){
            String username = data.optString("username");
            Chat.userUsernameMap.put(user, username);
            Chat.broadcastMessage(sender = "Server", msg = (username + " joined the chat"));
        }
        
    }

}
