package pregunta2.webSocket;

import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

import org.json.*;
import com.google.gson.Gson;

import pregunta2.util.Pair;
import pregunta2.model.OnlineGame;
import static pregunta2.App.onlineGameDao;

@WebSocket
public class BattlePlayWebSocket {

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        //Nothing to do really
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        Pair<Integer,String> userData = BattlePlay.userMap.get(user);
        BattlePlay.userMap.remove(userData);        
        System.out.println(userData.snd()+" disconnected ---");
        OnlineGame activeGame = onlineGameDao.findBySession(user);
        if(activeGame != null && activeGame.getStatus() == true)
            BattlePlay.alertDisconnection(activeGame, user);
        if(BattlePlay.onHold.contains(user))
            BattlePlay.onHold.remove(user);
    
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        OnlineGame activeGame = onlineGameDao.findBySession(user);
        JSONObject data = new JSONObject(message);
        String type = new String(data.optString("type"));
        System.out.println(type);
        if(type.equals("addToQueue")){
            Integer userId = data.optInt("user_id");
            String userName = data.optString("user_name");
            BattlePlay.addToQueue(user,userId,userName);
        }
        if(type.equals("answer") && activeGame!=null){
            Integer option = data.optInt("option");
            System.out.println(option);
            if(activeGame.isPlayer1(user)){
                activeGame.setAnswerPlayer1(option);
            }else if (activeGame.isPlayer2(user)){
                activeGame.setAnswerPlayer2(option);
            }else{                
                System.out.println("EL JUGADOR NO FORMA PARTE DEL JUEGO");
            }
            String result = activeGame.compareAnswers();
            String winner = activeGame.gameEnd();
            if(winner!="NotYet"){
                BattlePlay.sendEnd(activeGame,winner);
            }else if(result!="WaitingForAnswers"){
                BattlePlay.sendResult(activeGame,result);
            }
        }
        if(type.equals("question") && activeGame!=null && activeGame.isPlayer1(user)){
            BattlePlay.makeQuestion(activeGame);
        }
        if(type.equals(""))
            System.out.println("PORFAVOR QUE NO TE PASE ESTO XD");    
    }

}
