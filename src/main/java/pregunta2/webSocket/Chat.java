package pregunta2.webSocket;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import spark.Route;
import spark.Request;   
import spark.Response;

import org.eclipse.jetty.websocket.api.Session;

import org.json.*;
import com.google.gson.Gson;

import static j2html.TagCreator.*;
import static spark.Spark.*;
import static pregunta2.util.RequestUtil.*;
import pregunta2.util.ViewUtil;

public class Chat {

    // this map is shared between sessions and threads, so it needs to be thread-safe (http://stackoverflow.com/a/2688817)
    public static Map<Session, String> userUsernameMap = new ConcurrentHashMap<>();

    public static Route serveChat = (Request request, Response response) -> {
        if(!checkActiveUser(request)){
          response.redirect("/");
          return null;
        }
        Map model = new HashMap();
        model.put("userId",getSessionCurrentUserId(request));
        model.put("userName",getSessionCurrentUser(request));
        return ViewUtil.render(model,"chat/chat.mustache");      
    };

    //Sends a message from one user to all users, along with a list of current usernames
    public static void broadcastMessage(String sender, String message) {
        userUsernameMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("userMessage", createHtmlMessageFromSender(sender, message))
                    .put("userlist", userUsernameMap.values())
                ));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    //Builds a HTML element with a sender-name, a message, and a timestamp,
    private static String createHtmlMessageFromSender(String sender, String message) {
        return article(
            b(sender + " says:"),
            span(attrs(".timestamp"), new SimpleDateFormat("HH:mm:ss").format(new Date())),
            p(message)
        ).render();
    }

}
