package pregunta2.dao;

import pregunta2.model.OnlineGame;
import org.eclipse.jetty.websocket.api.Session;

import java.util.List;
import java.util.ArrayList;

import pregunta2.util.Pair;

public class OnlineGameDAO {

  public static List<OnlineGame> gamesList = new ArrayList<>();

	/**
   * Get all games from the database
   * 
   * @return list with all found games
   */
  public List<OnlineGame> getAllGames() {
    return gamesList;
  }

  /**
   * Search a OnlineGame in database
   * 
   * @param game id to search
   * 
   * @return OnlineGame wanted
   */
  public OnlineGame findById(Integer id){
    if (id != null) {
      for (int i=0;i<gamesList.size() ;i++) {
        OnlineGame game = gamesList.get(i);
        if(game.getId() == id){
          return game;
        }
      }
      return null;
    } else {
      return null;
    }
  }

  /**
   * Search a OnlineGame in database
   * 
   * @param player session to search
   * 
   * @return OnlineGame wanted
   */
  public OnlineGame findBySession(Session player){
    if (player != null) {
      for (int i=0;i<gamesList.size() ;i++) {
        OnlineGame game = gamesList.get(i);
        if(game.isPlayer1(player) || game.isPlayer2(player)){
          return game;
        }
      }
      return null;
    } else {
      return null;
    }
  }

 /**
   * Create a OnlineGame and add it to the List
   * 
   * @param id of the OnlineGame
   * @param player1 Session address
   * @param player1 Session address
   * @param player1Data Pair<Integer,String> that cointains the id and the username of the player1
   * @param player2Data Pair<Integer,String> that cointains the id and the username of the player2
   * 
   * @return boolean, true if the question was created
   */
  public OnlineGame createOnlineGame(Integer id, Session player1, Session player2, Pair<Integer,String> player1Data, Pair<Integer,String> player2Data){
    OnlineGame result;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = id == null || player1 == null || player2 == null || player1Data == null || player2Data == null;
    if(!areNull){
      areEmpty = (id < 1);
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createOnlineGame can't be null or empty.");
    } else {
      //if the question is already in database
      if(this.findById(id) != null){
        result = null;
      } else { //if not exist, register the new question in database
        OnlineGame create_game = new OnlineGame(id, player1, player2, player1Data, player2Data);
        gamesList.add(create_game);
        result = create_game;
      }
      return result;
    }
  }

  /**
   * Remove a game from the list
   * 
   * @param id from game to delete
   * 
   * @return boolean, true if the game was deleted
   */
  public boolean deleteGame(Integer id){
    if (id != null) {
      for (int i=0;i<gamesList.size() ;i++) {
        OnlineGame game = gamesList.get(i);
        if(game.getId() == id){
          gamesList.remove(i);
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  }

  /**
   * Remove a game from the list
   * 
   * @param player Session to search the game and delete it
   * 
   * @return boolean, true if the game was deleted
   */
  public boolean deleteGame(Session player){
    if (player != null) {
      for (int i=0;i<gamesList.size() ;i++) {
        OnlineGame game = gamesList.get(i);
        if(game.isPlayer1(player) || game.isPlayer2(player)){
          gamesList.remove(i);
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  }

    /**
   * Remove a game from the list
   * 
   * @param OnlineGame to delete
   * 
   * @return boolean, true if the game was deleted
   */
  public boolean deleteGame(OnlineGame game){
    if (game != null) {
      return deleteGame(game.getId());
    } else {
      return false;
    }
  }

}


