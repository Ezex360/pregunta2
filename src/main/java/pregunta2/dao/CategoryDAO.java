package pregunta2.dao;

import pregunta2.model.Category;

import java.util.List;

public class CategoryDAO {

  /**
   * Get all categories from the database
   * 
   * @return list with all found categories
   */
  public List<Category> getAllCategories() {
    List<Category> categories = Category.findAll();
    return categories;
  }

  /**
   * Search a Category in database
   * 
   * @param category id to search
   * 
   * @return Category wanted
   */
  public Category findById(Integer id){
    if (id != null) {
      Category category = Category.findFirst("id = ?",id);
      return category;
    } else {
      return null;
    }
  }

  /**
   * Search an category in database
   * 
   * @param category name to search
   * @return category wanted
   */
  public Category findByName(String name) {
    if(name == null || name.equals("")){
      throw new IllegalArgumentException("the 'name' param for search an category can not be null or empty.");
    } else {
      Category category = Category.findFirst("cat_name = ?", name);
      return category;
    }
  }

  /**
   * 
   * @param name
   * @param password
   * 
   * @return true if this Category exists in the database.
   */
  public boolean existCategory(String name){
    boolean result = false;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = name == null;
    if(!areNull){
      areEmpty = name.equals("");
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for search an Category can't be null or empty.");
    } else {
      //look for the category in the database
      Category category = Category.findFirst("cat_name = ?",name);
      if(category != null){
        result = true;
      }
    }
    return result;
  }

  /**
   * Create an category in the database
   * 
   * @param name, String that represents the username from an category
   * @param description from an category
   * 
   * @return boolean, true if the category was created
   */
  public boolean createCategory(String name, String description){
    boolean result;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = name == null || description == null;
    if(!areNull){
      areEmpty = name.equals("") || description.equals("");
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createCategory can't be null or empty.");
    } else {
      //if the category is already in database
      if(existCategory(name)){
        result = false;
      } else { //if not exist, register the new category in database
        Category category = new Category(name, description);
        category.save();
        result = true;
      }
      return result;
    }
  }

  /**
   * Remove a category from the database
   * 
   * @param id from category to delete
   * 
   * @return boolean, true if the category was deleted
   */
  public boolean deleteCategory(Integer id){
    Category category = this.findById(id);
    if (category != null) {
      category.delete();
      return true;
    } else {
      return false;
    }
  }

	
}
