package pregunta2.dao;

import pregunta2.model.Game;

import java.util.List;

public class GameDAO {

	/**
   * Get all games from the database
   * 
   * @return list with all found games
   */
  public List<Game> getAllGames() {
    List<Game> games = Game.findAll();
    return games;
  }

  /**
   * Search a Game in database
   * 
   * @param game id to search
   * 
   * @return Game wanted
   */
  public Game findById(Integer id){
    if (id != null) {
      Game game = Game.findFirst("id = ?",id);
      return game;
    } else {
      return null;
    }
  }

  /**
   * Search an game in database
   * 
   * @param user Integer that represents the user_id to search
   * @return game wanted
   */
  public List<Game> findByUserId(Integer user) {
    if(user == null){
      throw new IllegalArgumentException("the 'user' param for search an game can not be null or empty.");
    } else {
      List<Game> game = Game.where("user_id = ?", user);
      return game;
    }
  }

  /**
   * 
   * @param user Integer that represents the user_id to search
   * 
   * @return true if this Game exists in the database.
   */
  public Game activeGame(Integer user){
    if(user == null){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for search an Game can't be null or empty.");
    } else {
      //look for the game in the database
      Game game = Game.findFirst("user_id = ? and status = true",user);
      return game;
    }
    
  }

  /**
   * Create an game in the database only if the user does not have an active game
   * 
   * @param user, Integer that represents the user_id to search
   * 
   * @return boolean, true if the game was created
   */
  public boolean createGame(Integer user){
  	boolean result = false;
    if(user == null){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createGame can't be null or empty.");
    } else {
      //if the user is already playing a game
      if(activeGame(user) != null){
        result = false;
      } else { //if not exist, register the new game in database
        Game game = new Game(user);
        game.save();
        result = true;
      }
      return result;
    }
  }

  /**
   * Remove a game from the database
   * 
   * @param id from game to delete
   * 
   * @return boolean, true if the game was deleted
   */
  public boolean deleteGame(Integer id){
    Game game = this.findById(id);
    if (game != null) {
      game.delete();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Return the active Game of an user, in case the user don't have one
   * it creates it and returns the Game.
   * 
   * @param user_id to get the active game
   * 
   * @return Game, unique active Game of the user.
   */
  public Game getActiveGame(Integer user){
    createGame(user);
    return activeGame(user);
  }

  /**
   * Check if a game has 3 consecutives correct answers
   * 
   * @param game_id to search in DB
   * 
   * @return boolean, true if the game is onFire()
   */
  public boolean onFire(Integer game_id){
    if(game_id == null){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for search an Game can't be null or empty.");
    } else {
      //look for the game in the database
      Game game = Game.findFirst("id = ?",game_id);
      return game.onFire();
    }
  }

}


