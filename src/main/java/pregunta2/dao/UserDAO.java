package pregunta2.dao;

import pregunta2.model.User;
import pregunta2.model.History;
import pregunta2.util.Encryption;

import java.util.List;

public class UserDAO {

  /**
   * Get all users from the database
   * 
   * @return list with all found users
   */
  public List<User> getAllUsers() {
    List<User> users = User.findAll();
    return users;
  }

  /**
   * Search a User in database
   * 
   * @param user id to search
   * 
   * @return User wanted
   */
  public User findById(Integer id){
    if (id != null) {
      User user = User.findFirst("id = ?",id);
      return user;
    } else {
      return null;
    }
  }

  /**
   * Search an user in database
   * 
   * @param user username to search
   * @return user wanted
   */
  public User findByUsername(String username) {
    if(username == null || username.equals("")){
      throw new IllegalArgumentException("the 'username' param for search an user can not be null or empty.");
    } else {
      User user = User.findFirst("username = ?", username);
      return user;
    }
  }

  /**
   * 
   * @param name
   * @param password
   * 
   * @return true if this User exists in the database.
   */
  public boolean existUser(String name, String password){
    boolean result = false;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = name == null || password == null;
    if(!areNull){
      areEmpty = name.equals("") || password.equals("");
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for search an User can't be null or empty.");
    } else {
      //look for the user in the database
      String encriptedPassword = Encryption.encrypt(password);
      User user = User.findFirst("username = ? and password = ?",name,encriptedPassword);
      if(user != null){
        result = true;
      }
    }
    return result;
  }

  /**
   * Create an user in the database
   * 
   * @param name, String that represents the username from an user
   * @param password from an user
   * 
   * @return boolean, true if the user was created
   */
  public boolean createUser(String name, String password){
    boolean result;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = name == null || password == null;
    if(!areNull){
      areEmpty = name.equals("") || password.equals("");
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createUser can't be null or empty.");
    } else {
      //if the username is already in database
      if(this.findByUsername(name) != null){
        result = false;
      } else { //if not exist, register the new user in database
        String encriptedPassword = Encryption.encrypt(password);
        User user = new User(name, encriptedPassword);
        user.save();
        result = true;
      }
      return result;
    }
  }

  /**
   * Remove a user from the database
   * 
   * @param id from user to delete
   * 
   * @return boolean, true if the user was deleted
   */
  public boolean deleteUser(Integer id){
    User user = this.findById(id);
    if (user != null) {
      user.delete();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Method that returns the top 10 players of the game.
   * 
   * @return  List<User> that contains the best players ordered by score.
   */
  public List<User> getRanking() {
    String query = "SELECT * FROM users ORDER BY score DESC LIMIT 10";
    List<User> top10 = User.findBySQL(query); 
    return top10;
  }


}
