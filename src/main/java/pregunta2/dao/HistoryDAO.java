package pregunta2.dao;

import pregunta2.model.History;

import java.util.List;

public class HistoryDAO {


	/**
   * Get all histories from the database
   * 
   * @return list with all found histories
   */
  public List<History> getAllRecords() {
    List<History> histories = History.findAll();
    return histories;
  }

  /**
   * Search a History in database
   * 
   * @param history id to search
   * 
   * @return History wanted
   */
  public History findById(Integer id){
    if (id != null) {
      History history = History.findFirst("id = ?",id);
      return history;
    } else {
      return null;
    }
  }

  /**
   * Search histories by user_id in database
   * 
   * @param user Integer that represents the user_id to search
   * @return history wanted
   */
  public List<History> findByUserId(Integer user) {
    if(user == null){
      throw new IllegalArgumentException("the 'user' param for search an history can not be null or empty.");
    } else {
      List<History> history = History.where("user_id = ?", user);
      return history;
    }
  }

  /**
   * Search histories by game_id in database
   * 
   * @param game Integer that represents the game_id to search
   * @return history wanted
   */
  public List<History> findByGameId(Integer game) {
    if(game == null){
      throw new IllegalArgumentException("the 'game' param for search an history can not be null or empty.");
    } else {
      List<History> history = History.where("game_id = ?", game);
      return history;
    }
  }

  /**
   * Search histories by question_id in database
   * 
   * @param question Integer that represents the question_id to search
   * @return history wanted
   */
  public List<History> findByQuestionId(Integer question) {
    if(question == null){
      throw new IllegalArgumentException("the 'question' param for search an history can not be null or empty.");
    } else {
      List<History> history = History.where("question_id = ?", question);
      return history;
    }
  }

  /**
   * Add a history in the database
   * 
   * @param user Integer, represents the user_id
   * @param game Integer, represents the game_id
   * @param question Integer, represents the question_id
   * @param isCorrect Boolean, represents the correctness of the answer
   * 
   * @return boolean, true if the history was created
   */
  public boolean createRecord(Integer user, Integer game, Integer question, Boolean isCorrect){
  	boolean result;
    boolean areNull = false;
    areNull = question == null || game == null || question == null || user == null;
    if(areNull){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createRecord can't be null or empty.");
    } else {
      History record = new History(user, game, question, isCorrect);
      record.save();
      result = true;
    }
    return result;
  }
	
}
