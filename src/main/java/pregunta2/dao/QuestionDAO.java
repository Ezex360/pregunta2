package pregunta2.dao;

import pregunta2.model.Question;

import java.util.List;

public class QuestionDAO {

	/**
   * Get all questions from the database
   * 
   * @return list with all found questions
   */
  public List<Question> getAll() {
    List<Question> questions = Question.findAll();
    return questions;
  }

  /**
   * Search a Question in database
   * 
   * @param question id to search
   * 
   * @return Question wanted
   */
  public Question findById(Integer id){
    if (id != null) {
      Question question = Question.findFirst("id = ?",id);
      return question;
    } else {
      return null;
    }
  }

  /**
   * Search a question in database
   * 
   * @param question description to search
   * @return question wanted
   */
  public Question findByQuestion(String question) {
    if(question == null || question.equals("")){
      throw new IllegalArgumentException("the 'question' param for search an question can not be null or empty.");
    } else {
      Question questions = Question.findFirst("question = ?", question);
      return questions;
    }
  }

  /**
   * 
   * @param question
   * @param answer1
   * @param answer2
   * @param rightans 
   * 
   * @return true if this Question exists in the database.
   */
  public boolean existQuestion(String question, String answer1, String answer2, Integer rightans){
    boolean result = false;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = question == null || answer1 == null || answer2 == null || rightans == null;
    if(!areNull){
      areEmpty = question.equals("") || answer1.equals("") || answer2.equals("") || (rightans > 4 || rightans < 1);
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for search an Question can't be null or empty.");
    } else {
      //look for the question in the database
      List<Question> questions = Question
      .where("question = ? and answer1 = ? and answer2 = ? and rightans = ?",question,answer1,answer2,rightans);
      if(questions.size() > 0){
        result = true;
      }
    }
    return result;
  }

  /**
   * Create an question in the database
   * 
   * @param question String that represents the description of a question
   * @param answer1 String that represents the first option of the question
   * @param answer2 String that represents the second option of the question
   * @param rightans Integer that represents the number of the correct option for the question
   * 
   * @return boolean, true if the question was created
   */
  public boolean createQuestion(String question, String answer1, String answer2, Integer rightans){
    boolean result;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = question == null || answer1 == null || answer2 == null || rightans == null;
    if(!areNull){
      areEmpty = question.equals("") || answer1.equals("") || answer2.equals("") || (rightans > 4 || rightans < 1);
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createQuestion can't be null or empty.");
    } else {
      //if the question is already in database
      if(this.findByQuestion(question) != null){
        result = false;
      } else { //if not exist, register the new question in database
        Question create_question = new Question(question, answer1, answer2, rightans);
        create_question.save();
        result = true;
      }
      return result;
    }
  }

  /**
   * Create an question in the database
   * 
   * @param question String that represents the description of a question
   * @param answer1 String that represents the first option of the question
   * @param answer2 String that represents the second option of the question
   * @param answer3 String that represents the third option of the question
   * @param answer4 String that represents the fourth option of the question
   * @param rightans Integer that represents the number of the correct option for the question
   * 
   * @return boolean, true if the question was created
   */
  public boolean createQuestion(String question, String answer1, String answer2, String answer3, String answer4, Integer rightans){
    boolean result;
    boolean areEmpty = false;
    boolean areNull = false;
    areNull = question == null || answer1 == null || answer2 == null || answer3 == null || answer4 == null || rightans == null;
    if(!areNull){
      areEmpty = question.equals("") || answer1.equals("") || answer2.equals("") || answer3.equals("") || answer4.equals("") || (rightans > 4 || rightans < 1);
    }
    if(areNull || areEmpty){ //I see that the arguments are valid
      throw new IllegalArgumentException("The params for createQuestion can't be null or empty.");
    } else {
      //if the question is already in database
      if(this.findByQuestion(question) != null){
        result = false;
      } else { //if not exist, register the new question in database
        Question create_question = new Question(question, answer1, answer2, answer3, answer4, rightans);
        create_question.save();
        result = true;
      }
      return result;
    }
  }

  /**
   * Remove a question from the database
   * 
   * @param id from question to delete
   * 
   * @return boolean, true if the question was deleted
   */
  public boolean deleteQuestion(Integer id){
    Question question = this.findById(id);
    if (question != null) {
      question.delete();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Get a random Question
   * 
   * @return Question, random question
   */
  public static Question randomQuestion(){
    String query ="SELECT * FROM questions Order by rand() LIMIT 1";
    List<Question> randomCategoryQuestion = Question.findBySQL(query);
    return randomCategoryQuestion.get(0);
  }


  /**
   * Get a random Question from a determinated category
   * 
   * @param cat_id of the category to obtain the random question
   * 
   * @return Question, random question
   */
  public static Question randomQuestion(Integer cat_id){
    String query = "SELECT * FROM questions where (category_id = ?) Order by rand() LIMIT 1";
    List<Question> randQuestion = Question.findBySQL(query,cat_id);
    return randQuestion.get(0);
  }


  /**
   * Get a random Question that the user has not answered in his active Game, for a determinated
   * category
   * 
   * @param cat_id of the category to obtain the random question
   * @param user_id of the active gameto search unaswered questions
   * 
   * @return Question, random question
   */
  public static Question randomQuestion(Integer cat_id,Integer user_id){
    String query ="SELECT * FROM questions where (category_id = ?) and questions.id not in "+
    "(select question_id from histories join games on (game_id = games.id) "+
    "where (status = true and histories.user_id = ?)) Order by rand() LIMIT 1";
    List<Question> unansweredQuestions = Question.findBySQL(query,cat_id,user_id);
    Question question;
    if (!unansweredQuestions.isEmpty()){
      question = unansweredQuestions.get(0);
    }else{
      question = randomQuestion(cat_id);
    }
    return question;
  }

}
