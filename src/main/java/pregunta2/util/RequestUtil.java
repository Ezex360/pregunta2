package pregunta2.util;

import spark.*;

public class RequestUtil {

    public static String getQueryUsername(Request request) {
        return request.queryParams("username");
    }

    public static String getQueryPassword(Request request) {
        return request.queryParams("password");
    }

    public static Integer getParamQuestionId(Request request) {
        return Integer.parseInt(request.params(":question_id").substring(1));
    }

    public static String getSessionCurrentUser(Request request) {
        return request.session().attribute("currentUser");
    }

    public static Integer getSessionCurrentUserId(Request request) {
        return request.session().attribute("currentUserId");
    }

    public static Integer getSessionCurrentGameId(Request request) {
        return request.session().attribute("currentGameId");
    }

    public static boolean checkActiveUser(Request request) {
        boolean activeUser = request.session().attribute("currentUser") != null;
        boolean activeUserId = request.session().attribute("currentUserId") != null;
        return activeUserId && activeUser;
    }

    public static boolean checkActiveGame(Request request) {
        boolean activeUser = request.session().attribute("currentUserId") != null;
        boolean activeGame = request.session().attribute("currentGameId") != null;
        return activeGame && activeUser;
    }


    public static String removeSessionAttrCurrentUser(Request request) {
        String currentUser = request.session().attribute("currentUser");
        request.session().removeAttribute("currentUser");
        return currentUser;
    }

    public static Integer removeSessionAttrCurrentUserId(Request request) {
        Integer currentUser = request.session().attribute("currentUserId");
        request.session().removeAttribute("currentUserId");
        return currentUser;
    }

    public static Integer removeSessionAttrCurrentGameId(Request request) {
        Integer currentUser = request.session().attribute("currentGameId");
        request.session().removeAttribute("currentGameId");
        return currentUser;
    }


}
