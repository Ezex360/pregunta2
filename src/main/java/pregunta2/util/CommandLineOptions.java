package pregunta2.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * 
 * @author Dose 
 *
 */
public class CommandLineOptions {

    public static void parseOptions(String[] args) {

    	CommandLineParser parser = new DefaultParser();
    	
    	Option dbHost = new Option("dbh","dbHost",true,"use given host as database host");
    	Option dbPort = new Option("dbp","dbPort",true,"use given port as database port");
    	Option appPort =  new Option("ap","appPort",true,"use given port as application port");
        Option dbUser =  new Option("dbu","dbUser",true,"use given user as username for database");
        Option dbPass =  new Option("dbps","dbPass",true,"use given user as password for user of the database");

    	appPort.setRequired(false);

    	Options options = new Options();
    	options.addOption(dbHost);
    	options.addOption(dbPort);
    	options.addOption(appPort);
        options.addOption(dbUser);
        options.addOption(dbPass);
        try {
            // parse the command line arguments
            CommandLine line = parser.parse( options, args );
            if (line.hasOption("dbHost")) {
            	pregunta2.ServerOptions.getInstance().setDbHost(line.getOptionValue("dbHost"));
            }
            if (line.hasOption("dbPort")) {
            	pregunta2.ServerOptions.getInstance().setDbPort(line.getOptionValue("dbPort"));
            }
            if (line.hasOption("appPort")) { 
            	pregunta2.ServerOptions.getInstance().setAppPort(Integer.parseInt(line.getOptionValue("appPort")));            
            }else{
                ProcessBuilder processBuilder = new ProcessBuilder();
                if (processBuilder.environment().get("PORT") != null)
                    pregunta2.ServerOptions.getInstance().setAppPort(Integer.parseInt(processBuilder.environment().get("PORT")));
                else
                    pregunta2.ServerOptions.getInstance().setAppPort(4567);
            }
            if (line.hasOption("dbUser")){  
                pregunta2.ServerOptions.getInstance().setAppPort(Integer.parseInt(line.getOptionValue("appPort")));
            }
            if (line.hasOption("dbPass")){ 
                pregunta2.ServerOptions.getInstance().setAppPort(Integer.parseInt(line.getOptionValue("appPort")));
            }
        }
        catch( ParseException exp ) {
            // oops, something went wrong
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }
    }
}
