package pregunta2;

import org.javalite.activejdbc.Base;

public class ServerOptions {


	/**
	 * Empty, private default constructor.
	 */
	private ServerOptions() { }
	
	/**
	 * Singleton, on demand instance of ServerOptions.
	 */
	private static ServerOptions instance = null;
	
	/**
	 * Server option that holds the hosts for the database.
	 * Default value is "localhost", i.e., assumes that database
	 * runs on the same host as the server.
	 */
	private String dbHost = "localhost";
	
	/**
	 * Server option that holds the port where the database
	 * server listens for clients. Default value is "3306", i.e.
	 * the default port for MySQL server.
	 */
	private String dbPort = "3306";
	
	/**
	 * Server option that holds the port where the application
	 * server listens for clients. Default value is "4567".
	 */
	private Integer appPort = 4567;

	/**
	 * Server option that holds the user for the database.
	 * Default value is "proyecto", i.e., assumes that database
	 * runs on the same host as the server.
	 */
	private String dbUser = "proyecto";

	/**
	 * Server option that holds the pasword of the user for the database.
	 * Default value is "felipe", i.e., assumes that database
	 * runs on the same host as the server.
	 */
	private String dbPass = "felipe";

	/**
	 * Returns the (sole) instance of ServerOptions, on demand.
	 * It is created the first time it is accessed, and from that
	 * point onwards, the same instance is returned.
	 * See the Singleton Pattern for reference.
	 * @return the instance of ServerOptions.
	 */
	public static ServerOptions getInstance() {
		if (instance==null) instance = new ServerOptions();
		return instance;
	}
	
	/**
	 * Sets the database host, that the application communicates with.
	 * @param dbHost is the new database host
	 */
	public void setDbHost(String dbHost) {
		if (dbHost==null || dbHost=="") throw new IllegalArgumentException("invalid hostname");
		this.dbHost = dbHost;
	}
	
	/**
	 * Sets the database port, where the application locates the database server it
	 * communicates with.
	 * @param dbPort is the new database port
	 */
	public void setDbPort(String dbPort) {
		this.dbPort = dbPort;
	}
	
	/**
	 * Sets the application port, that is used to enter to the application.
	 * @param appPort is the new application port
	 */
	public void setAppPort(Integer appPort) {
		this.appPort = appPort;	
	}

	/**
	 * Sets the database user, that is used to enter to the database.
	 * @param dbUser is the new database user username
	 */
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;	
	}

	/**
	 * Sets the password of the user is used to access the database
	 * @param dbPass is the new database user password
	 */
	public void setDbPass(String dbPass) {
		this.dbPass = dbPass;	
	}
	
	/**
	 * Returns the password of the user of the database.
	 * @return password of the user.
	 */
	public String getDbPass() {
		return this.dbPass;
	}
	
	/**
	 * Returns the username of the user of the database.
	 * @return username of the user.
	 */
	public String getDbUser() {
		return this.dbUser;
	}
	
	/**
	 * Returns the host where the database that the application communicates with, resides.
	 * @return the host where the database server is running
	 */
	public String getDbHost() {
		return this.dbHost;
	}
	
	/**
	 * Returns the port where the database server that the application communicates with, is listening.
	 * @return the port where the database server is listening.
	 */
	public String getDbPort() {
		return this.dbPort;
	}
	
	/**
	 * Returns the port where the application is listening.
	 * @return the port where the application is listening.
	 */
	public Integer getAppPort() {
		return this.appPort;
	}

	/**
	 * Method to open the DataBase using the info of this class
	 */
	public void openDB() {
		Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://"+this.dbHost+":"+this.dbPort+"/pregunta2", dbUser, dbPass);
	}

	/**
	 * Method to close the DataBase
	 */
	public void closeDB() {
		Base.close();
	}
	
	
	
}
